<?php

namespace Drupal\openfed_migrate;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Database\Schema;
use Drupal\migrate\Row;

/**
 * Set of functions to be used on migrations.
 */
class Helper {

  /*
   * This function should be used on source query() functions in order to update
   * the query with entity checks.
   *
   * This is necessary because some entity bundles are wrongly imported just
   * because they are set in features but not in the database.
   *
   * @param object $query
   *   The query to alter.
   * @param string $bundle_column
   *   The column to use to query for bundle type.
   *
   * @return void
   *
   */
  static public function sourceQueryAlter(SelectInterface &$query, Schema $schema, $bundle_column = 'fci.bundle', $entity_column = 'fci.entity_type') {
    // Left join node types to later check if they exist.
    $query->leftJoin('node_type', 'node_type', "$bundle_column = node_type.type");

    // Check if types are empty.
    $nodeOrGroup = $query->orConditionGroup()
      ->condition($entity_column, ['node'], 'NOT IN')
      ->condition('node_type.type', NULL, 'IS NOT NULL');

    // We create an "And" condition to group the several entity types that may
    // require checking.
    $andGroup = $query->andConditionGroup()
      ->condition($nodeOrGroup);

    // Left join bean types to later check if they exist. We additionally add a
    // check to see if there is bean content because in some cases bean_type
    // table is empty and entity type info is in features, allowing content to
    // be added.
    if ($schema->tableExists('bean_type') && $schema->tableExists('bean')) {
      $query->leftJoin('bean_type', 'bean_type', "$bundle_column = bean_type.name");
      $query->leftJoin('bean', 'bean', "$bundle_column = bean.type");

      // Check if types are empty.
      $beanOrGroup = $query->orConditionGroup()
        ->condition($entity_column, ['bean'], 'NOT IN')
        ->condition('bean.type', NULL, 'IS NOT NULL');

      $andGroup->condition($beanOrGroup);
    }

    // Add the group to the query.
    $query->condition($andGroup);

    // Add a distinct parameter to avoid duplicates.
    $query->distinct();
  }

  /*
   * This function should be used on source prepareRow() functions in order to
   * validate row entity bundles (added by sourceQueryAlter()).
   *
   * This is necessary because some entity bundles are wrongly imported just
   * because they are set in features but not in the database.
   *
   * @param object $row
   *   The row with source values to validate.
   * @param string $source_entity_key
   *   The source property containing the entity type.
   *
   * @return bool
   *   False if the row should be skipped from migration.
   *
   */
  static public function sourcePrepareRowBundleValidator(Row $row, $source_entity_key) {
    //    $entity_types = [
    //      'bean',
    //      'node',
    //    ];
    //    if (in_array($row->getSourceProperty($source_entity_key), $entity_types)) {
    //      if (empty($row->getSourceProperty('node_type')) && empty($row->getSourceProperty('bean_type'))) {
    //        return FALSE;
    //      }
    //    }

    return TRUE;
  }

  /**
   * Enables text processing for multiple instances of a text_long.
   *
   * @see https://www.drupal.org/docs/8/upgrade/known-issues-when-upgrading-from-drupal-6-or-7-to-drupal-8#plain-text
   *
   * @param Row $row
   *   The row with source values to validate.
   *
   * @throws \Exception
   */
  static public function enableTextProcessingOnMultipleInstances(Row $row) {
    if ($row->getSourceProperty('type') === 'text_long') {
      $instances = $row->getSourceProperty('instances');
      if (!empty($instances) && count($instances) > 1) {
        foreach ($instances as $key => $instance) {
          $instance_settings = unserialize($instance['data']);
          $instance_settings['settings']['text_processing'] = TRUE;
          $instances[$key]['data'] = serialize($instance_settings);
        }
        $row->setSourceProperty('instances', $instances);
      }
    }
  }

  /**
   * Gets all country codes and names from the countries module.
   *
   * @return array
   *   All countries keyed.
   */
  public static function getCountries() {
    Database::setActiveConnection('migrate');

    $query = Database::getConnection()->select('countries_country', 'c');
    $query->fields('c', ['iso2', 'name']);
    $countries = $query->execute()->fetchAllKeyed();

    Database::setActiveConnection();
    return $countries;
  }

  /**
   * Check if a migration row is a simple_gmap field.
   *
   * The only way can check here if there is a formatter using the simple_gmap
   * module, is checking the displays.
   *
   * @param string $field_name
   *   The name of the field.
   *
   * @return bool
   *   TRUE if it's a simple_gmap field, otherwise FALSE.
   */
  public static function isMigrationFieldRowSimpleGmap($field_name) {
    Database::setActiveConnection('migrate');
    $connection = Database::getConnection();
    Database::setActiveConnection();

    // We must check if display suite is enabled. If that's the case, the config
    // of this module will override the default field instance config.
    $is_ds_enabled = Helper::isDsEnabled();

    $query = $connection->select('field_config_instance', 'fci');
    $query->fields('fci', ['field_name', 'entity_type', 'bundle', 'data']);
    $query->condition('field_name', $field_name);
    $query->where("CONVERT(data USING utf8) LIKE '%simple_gmap%'");
    $simple_map_fields = $query->execute()->fetchAll();

    foreach ($simple_map_fields as $field_data) {
      $data = unserialize($field_data->data);
      foreach ($data['display'] as $display_name => $display_data) {
        if ($display_data['type'] == 'simple_gmap') {
          if ($is_ds_enabled) {
            $query = $connection->select('ds_layout_settings', 'dls');
            $query->fields('dls', ['settings']);
            $query->condition('entity_type', $field_data->entity_type);
            $query->condition('bundle', $field_data->bundle);
            $query->condition('view_mode', $display_name);
            $ds_field_settings = $query->execute()->fetchField();

            $settings = unserialize($ds_field_settings);
            if (isset($settings['fields'][$field_name])) {
              return TRUE;
            }
          }
          else {
            return TRUE;
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * Check if Display Suite is enabled.
   *
   * @return bool
   *   TRUE if Display Suite is enabled, FALSE otherwise.
   */
  public static function isDsEnabled() {
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('ds')) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get Country code for a given address, using nominatim API.
   *
   * @param string $address
   *   The address to search for.
   *
   * @return string
   *   The country code.
   *    default country code: BE.
   */
  public static function getCountry(string $address) {
    $url = "https://nominatim.openstreetmap.org/search?q=$address&format=jsonv2&addressdetails=1";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    $output = curl_exec($ch);
    curl_close($ch);

    $response = json_decode($output)[0];

    if ($response && !empty($response->address) && !empty($response->address->country_code)) {
      return strtoupper($response->address->country_code);
    }

    return 'BE';
  }


  /**
   * Check if a vocabulary is translatable.
   *
   * @param string $vocabulary
   *   The vocabulary name to check.
   *
   * @return boolean
   *   TRUE is the vocabulary is translatable, FALSE otherwise.
   */
  public static function vocabularyIsTranslatable($vocabulary) {
    Database::setActiveConnection('migrate');

    $query = Database::getConnection()->select('taxonomy_vocabulary', 'tv');
    $query->condition('tv.i18n_mode', 4);
    $query->condition('tv.machine_name', $vocabulary);
    $result = $query->countQuery()->execute()->fetchField();

    Database::setActiveConnection();

    return ($result > 0) ? TRUE : FALSE;
  }

  /**
   * Get a list of taxonomy fields that should be translatable.
   *
   * @param string $vocabulary
   *   The vocabulary name to check.
   *
   * @return array
   *   The array of fields that belong to a translated vocabulary.
   */
  public static function getTaxonomyTranslatableFields($vocabulary) {
    Database::setActiveConnection('migrate');

    $query = Database::getConnection()->select('taxonomy_vocabulary', 'tv');
    $query->condition('tv.i18n_mode', 4);
    $query->join('field_config_instance', 'fci', 'tv.machine_name=fci.bundle');
    $query->fields('fci', ['id','field_name']);
    $query->condition('tv.machine_name', $vocabulary);
    $result = $query->execute();

    Database::setActiveConnection();

    return array_values($result->fetchAllKeyed());
  }

  /**
   * Get a list of long text fields.
   *
   * @return array
   *   The array of long text fields.
   */
  public static function getLongTextFields() {
    Database::setActiveConnection('migrate');

    $query = Database::getConnection()->select('field_config', 'fc');
    $query->condition('fc.module', 'text');
    $query->condition('fc.type', ['text_long', 'text_with_summary'], 'IN');
    // TODO: make this an optional condition. Right now, other cardinalities
    // will pose some issues.
    $query->condition('fc.cardinality', 1);
    $query->fields('fc', ['field_name']);
    $result = $query->execute();

    Database::setActiveConnection();

    return $result->fetchAllKeyed();
  }

  /**
   * Get a single file info, identified by its id, from D7 database.
   *
   * @param string $file_id
   *   The file to retrieve info from.
   *
   * @return object
   *   The complete file_managed row from D7 for the file. FALSE if no results.
   */
  public static function fileInfoLoad(string $file_id) {

    Database::setActiveConnection('migrate');

    $query = Database::getConnection()->select('file_managed', 'fm');
    $query->fields('fm');
    $query->condition('fid', $file_id);
    $file_info = $query->execute()->fetch();

    Database::setActiveConnection();
    return $file_info;
  }

  /**
   * Alter menu link row.
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   */
  public static function alterMenuLinkRow(Row $row) {
    $link_path = $row->getSourceProperty('link_path');

    if ($link_path === '<nolink>') {
      $row->setSourceProperty('link_path', 'route:<nolink>');
    }
    else if ($link_path === '<firstchild>') {
      $options = $row->getSourceProperty('options') ?? '';

      if ($options !== '') {
        $options = unserialize($options);
        $options['menu_firstchild'] = ['enabled' => 1];
        $row->setSourceProperty('options', serialize($options));
        $row->setSourceProperty('link_path', 'route:<none>');
      }
    }
  }

  /**
   * Returns all non-deleted field instances attached to a specific entity type.
   *
   * @param string $entity_type
   *   The entity type ID.
   * @param string|null $bundle
   *   (optional) The bundle.
   *
   * @return array[]
   *   The field instances, keyed by field name.
   *
   * @see FieldableEntity::getFields()
   */
  public static function getFields($entity_type, $bundle = NULL) {
    Database::setActiveConnection('migrate');

    $query = Database::getConnection()->select('field_config_instance', 'fci')
      ->fields('fci')
      ->condition('fci.entity_type', $entity_type)
      ->condition('fci.bundle', isset($bundle) ? $bundle : $entity_type)
      ->condition('fci.deleted', 0);

    // Join the 'field_config' table and add the 'translatable' setting to the
    // query.
    $query->leftJoin('field_config', 'fc', 'fci.field_id = fc.id');
    $query->addField('fc', 'translatable');

    $result = $query->execute()->fetchAllAssoc('field_name');

    Database::setActiveConnection();

    return $result;
  }

  /**
   * Retrieves field values for a single field of a single entity.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $field
   *   The field name.
   * @param int $entity_id
   *   The entity ID.
   * @param int|null $revision_id
   *   (optional) The entity revision ID.
   * @param string $language
   *   (optional) The field language.
   *
   * @return array
   *   The raw field values, keyed by delta.
   *
   * @see @see FieldableEntity::getFieldValues()
   */
  public static function getFieldValues($entity_type, $field, $entity_id, $revision_id = NULL, $language = NULL) {
    Database::setActiveConnection('migrate');

    $table = (isset($revision_id) ? 'field_revision_' : 'field_data_') . $field;
    $query = Database::getConnection()->select($table, 't')
      ->fields('t')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->condition('deleted', 0);
    if (isset($revision_id)) {
      $query->condition('revision_id', $revision_id);
    }
    // Add 'language' as a query condition if it has been defined by Entity
    // Translation.
    if ($language) {
      $query->condition('language', $language);
    }
    $values = [];
    foreach ($query->execute() as $row) {
      foreach ($row as $key => $value) {
        $delta = $row->delta;
        if (strpos($key, $field) === 0) {
          $column = substr($key, strlen($field) + 1);
          $values[$delta][$column] = $value;
        }
      }
    }

    Database::setActiveConnection();

    return $values;
  }

  /*
   * This function will remove unnecessary field from the migration.
   *
   * @param SelectInterface $query
   *   The query to alter.
   *
   * @return void
   *
   */
  public static function ignoreFields(SelectInterface &$query) {
      $fields_to_ignore = [
        'field_file_image_alt_text',
        'field_file_image_title_text',
      ];
      $query->condition('fc.field_name', $fields_to_ignore, 'NOT IN');
  }

}
