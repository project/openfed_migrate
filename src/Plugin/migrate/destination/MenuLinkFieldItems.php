<?php

namespace Drupal\openfed_migrate\Plugin\migrate\destination;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @MigrateDestination(
 *   id = "menu_link_field_items"
 * )
 */
class MenuLinkFieldItems extends DestinationBase implements ContainerFactoryPluginInterface {

  /**
   * The storage handler class for nodes.
   *
   * @var \Drupal\node\NodeStorage
   */
  private $nodeStorage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLink;

  /**
   * Constructs an entity destination plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity
   *   The Entity type manager service.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menuLink
   *   The Menu Link plugin manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, EntityTypeManagerInterface $entity, MenuLinkManagerInterface $menuLink) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->nodeStorage = $entity->getStorage('node');
    $this->menuLink = $menuLink;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.menu.link')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $source_nid = $row->getDestinationProperty('nid');

    if (empty($source_nid)) {
      return FALSE;
    }

    // If $source_nid is an array, it means that it contains the language,
    // besides the node id. Langcode is at index 1 while nid is at index 0.
    if (is_array($source_nid)) {

        // Sometime $source_nid is an array of null item,
        // filter the array to remove nulls
        $source_nid = array_filter($source_nid);
        if( empty($source_nid)){
            return false;
        }

      $node = $this->nodeStorage->load($source_nid[0])
        ->getTranslation($source_nid[1]);
    }
    else {
      $node = $this->nodeStorage->load($source_nid);
    }

    if ($node && $node->hasField('field_menulink')) {
      $node->field_menulink = [
        'menu_name' => $row->getDestinationProperty('menu_name'),
        'title' => $row->getDestinationProperty('title'),
        'description' => $row->getDestinationProperty('description'),
        'parent' => $row->getDestinationProperty('parent'),
        'weight' => $row->getDestinationProperty('weight'),
      ];
      $node->setNewRevision(TRUE);
      $node->setRevisionLogMessage('Update menu link');
      $node->isDefaultRevision(TRUE);
      $this->nodeStorage->save($node);

      // Update the menu link if it is unpublished/hidden. "True" means the link
      // is unpublished.
      if ($row->getDestinationProperty('enabled')) {
        $id = 'menu_link_field:node_field_menulink_' . $node->uuid() . '_' . $node->language()->getId();
        $menu_link = $this->menuLink->loadLinksByRoute('entity.node.canonical',['node'=> $node->id()]);
        if(isset($menu_link[$id])) {
          $this->menuLink->updateDefinition($id, ['enabled' => 0]);
        }
      }

      return [$row->getDestinationProperty('id'), $row->getDestinationProperty('menu_name')];
    }

    return FALSE;

  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['mlid']['type'] = 'integer';
    $ids['menu_name']['type'] = 'string';
    return $ids;
  }

  /**
   * @inheritDoc
   */
  public function fields(MigrationInterface $migration = NULL) {
    // TODO: Implement fields() method.
  }

}
