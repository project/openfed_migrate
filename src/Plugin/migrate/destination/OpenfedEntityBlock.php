<?php

namespace Drupal\openfed_migrate\Plugin\migrate\destination;

use Drupal\block\Plugin\migrate\destination\EntityBlock;
use Drupal\migrate\Row;

/**
 * @MigrateDestination(
 *   id = "entity:block"
 * )
 */
class OpenfedEntityBlock extends EntityBlock {

  /**
   * {@inheritdoc}
   */
  protected function getEntityId(Row $row) {
    $source_module = $row->getSourceProperty('module');

    switch ($source_module) {
      case 'menu_block':
        $menu_block_id = 'menu_block_' . $row->getSourceProperty('delta');
        // We will directly use the block ID to load it if it is a menu_block.
        $properties = [
          'plugin' => $row->getDestinationProperty('plugin'),
          'theme' => $row->getDestinationProperty('theme'),
          'id' => $menu_block_id,
        ];
        break;
      default:
        // Try to find the block by its plugin ID and theme.
        $properties = [
          'plugin' => $row->getDestinationProperty('plugin'),
          'theme' => $row->getDestinationProperty('theme'),
        ];
    }


    $blocks = array_keys($this->storage->loadByProperties($properties));
    return reset($blocks);
  }

}
