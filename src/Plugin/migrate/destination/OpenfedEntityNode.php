<?php

namespace Drupal\openfed_migrate\Plugin\migrate\destination;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Row;

/**
 * Overrides node destination.
 *
 * @MigrateDestination(
 *   id = "entity:node"
 * )
 */
class OpenfedEntityNode extends EntityContentBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\migrate\MigrateException
   *   When an entity cannot be looked up.
   * @throws \Drupal\migrate\Exception\EntityValidationException
   *   When an entity validation hasn't been passed.
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $this->rollbackAction = MigrateIdMapInterface::ROLLBACK_DELETE;
    $entity = $this->getEntity($row, $old_destination_id_values);
    if (!$entity) {
      throw new MigrateException('Unable to get entity');
    }
    assert($entity instanceof ContentEntityInterface);
    if ($this->isEntityValidationRequired($entity)) {
      $this->validateEntity($entity);
    }

    // The custom code to handle node revisions.
    // Without this, Drupal won't update content and will throw an error.

    // Temporary until there is a fix at
    // https://www.drupal.org/project/drupal/issues/2859042.
    if ($entity->getEntityType()->isRevisionable()
      && $entity->getLoadedRevisionId()
      && $entity->getRevisionId() != $entity->getLoadedRevisionId()) {
      $entity->original = $this->storage->loadRevision($entity->getLoadedRevisionId());
      $entity->setNewRevision();
    }

    // This like will avoid path alias to be generated for migrations.
    // Like this we are sure to keep aliases between migrations.
    $entity->path->pathauto = 0;

    $ids = $this->save($entity, $old_destination_id_values);
    if ($this->isTranslationDestination()) {
      $ids[] = $entity->language()->getId();
    }
    return $ids;
  }

}
