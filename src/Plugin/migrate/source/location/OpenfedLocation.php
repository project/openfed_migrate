<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\location;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Plugin\MigrateSourceInterface;

/**
 * A location source plugin.
 *
 * @MigrateSource(
 *   id = "d7_location",
 *   core = {7},
 *   type_map = {
 *     "location" = "geolocation"
 *   },
 *   source_module = "location",
 *   destination_module = "geolocation"
 * )
 *
 */
class OpenfedLocation extends SqlBase implements MigrateSourceInterface {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Select node in its last revision.
    $query = $this->select('location_instance', 'li')
      ->fields('li', [
        'nid',
        'vid',
        'uid',
        'genid',
        'lid',
      ])
      ->fields('l', [
        'lid',
        'name',
        'street',
        'additional',
        'city',
        'province',
        'postal_code',
        'country',
        'latitude',
        'longitude',
        'source',
        'is_primary',
      ])
      ->fields('n', [
        'title',
        'language',
      ]);
    $query->innerJoin('location', 'l', 'l.lid = li.lid');
    $query->innerJoin('node', 'n', 'n.nid = li.nid');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'nid' => $this->t('Node ID'),
      'vid' => $this->t('Version ID'),
      'uid' => $this->t('User ID'),
      'genid' => $this->t('Gen ID'),
      'lid' => $this->t('Location ID'),
      'name' => $this->t('Location Name'),
      'street' => $this->t('Location Street'),
      'additional' => $this->t('Location Additional'),
      'city' => $this->t('Location City'),
      'province' => $this->t('Location Province'),
      'postal_code' => $this->t('Location Postal Code'),
      'country' => $this->t('Location Country'),
      'latitude' => $this->t('Location Latitude'),
      'longitude' => $this->t('Location Longitude'),
      'source' => $this->t('Location Source'),
      'is_primary' => $this->t('Location Is Primary'),
      'title' => $this->t('Node Title'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'nid' => [
        'type' => 'integer',
      ],
      'lid' => [
        'type' => 'integer',
      ],
    ];
  }

}
