<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\menu_block_settings_override;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\MigrateLookupInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal menu block settings override source from database.
 *
 * @MigrateSource(
 *   id = "menu_block_settings_override_items",
 *   source_module = "menu_block_settings_override"
 * )
 */
class MenuBlockSettingsOverrideItems extends DrupalSqlBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The migration plugin manager service.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * The migrate lookup service.
   *
   * @var \Drupal\migrate\MigrateLookupInterface
   */
  protected $migrateLookup;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, MigrationPluginManagerInterface $migration_plugin_manager, MigrateLookupInterface $migrate_lookup) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
    $this->moduleHandler = $module_handler;
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->migrateLookup = $migrate_lookup;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('plugin.manager.migration'),
      $container->get('migrate.lookup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // This is not a field migration because menu_block_settings_override has
    // its own table in D7.
    $query = $this->select('menu_block_settings_override', 'mbso')
      ->fields('mbso');

    // We do a left join to get all the nodes in the above conditions.
    $query->join('node', 'n', "mbso.path LIKE CONCAT('node/', n.nid)");
    $query->fields('n', ['type', 'nid', 'language', 'tnid']);

    // We can exclude nodes that were never overriden or hidden. In D7, MBSO was
    // implemented on all content types but not all content types were really
    // making use of it.
    $orGroup = $query->orConditionGroup()
      ->condition('mbso.status', 0, '<>')
      ->condition('mbso.hidden', 0, '<>');
    // Add the group to the query.
    $query->condition($orGroup);

    // If the content_translation module is enabled, get the source langcode
    // to fill the content_translation_source field.
    if ($this->moduleHandler->moduleExists('content_translation')) {
      $query->leftJoin('node', 'nt', 'n.tnid = nt.nid');
      $query->addField('nt', 'language', 'source_langcode');
    }
    $this->handleTranslations($query);

    return $query;
  }

  /**
   * Adapt our query for translations.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The generated query.
   */
  protected function handleTranslations(SelectInterface $query) {
    // Check whether or not we want translations.
    if (!isset($this->migration->getDestinationConfiguration()['translations'])) {
      // No translations: Yield untranslated nodes, or default translations.
      $query->where('n.tnid = 0 OR n.tnid = n.nid');
    }
    else {
      // Translations: Yield only non-default translations.
      $query->where('n.tnid <> 0 AND n.tnid <> n.nid');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $settings = unserialize($row->getSourceProperty('config'));
    $hidden_option = $row->getSourceProperty('hidden');
    $status_option = $row->getSourceProperty('status');
    $block_delta = $row->getSourceProperty('delta');

    $row->setDestinationProperty('default_bundle', $row->getSourceProperty('type'));
    $row->setDestinationProperty('bundle', $row->getSourceProperty('type'));

    // We will try to get the parent menu link id.
    // Menu link migration is tricky, specially if we are migrating using do a
    // menu_link field (d7_menu_link_items), which is the case of node links.
    // We'll first identify the process responsible for the migration and then
    // we act accordingly.
    $menu_parent_mlid = NULL;
    $migration_plugin_definitions_batch1 = [
      'upgrade_d7_menu_links',
      'upgrade_d7_menu_links_final_fixes',
      'upgrade_node_translation_menu_links',
    ];
    $migration_plugin_definitions_batch2 = [
      'upgrade_d7_menu_link_items',
    ];

    //TODO: process batch 1. Should be easier...
    foreach ($migration_plugin_definitions_batch2 as $plugin_definition) {
      if ($this->migrationPluginManager->hasDefinition($plugin_definition)) {
        $parent_mlid = $settings['parent_mlid'];
        $mlids = $this->migrateLookup->lookup($plugin_definition, ['mlid' => $parent_mlid]);
        if ($mlids) {
          // Due to the way menu_links are being migrated, we need to query D7
          // database in order to retrieve the necessary node info.
          Database::setActiveConnection('migrate');
          $query = Database::getConnection()->select('menu_links', 'ml');
          $query->join('node', 'n', "ml.link_path LIKE CONCAT('node/', n.nid)");
          $query->fields('n', ['nid', 'language', 'type']);
          $query->condition('mlid', $parent_mlid); // primary key
          $node_data = $query->execute()->fetchAssoc();
          Database::setActiveConnection();

          // Now we also need to lookup for the migrated node.
          $migration_plugin_definitions = [
            'd7_node_' . $node_data['type'],
            'upgrade_d7_node_' . $node_data['type'],
            'd7_node_translation_' . $node_data['type'],
            'upgrade_d7_node_translation_' . $node_data['type'],
          ];
          // We do a lookup through the migration map
          // table in order to get the destination node ID
          // and if found, then we return the menu link pattern.
          foreach ($migration_plugin_definitions as $node_plugin_definition) {
            if ($this->migrationPluginManager->hasDefinition($node_plugin_definition)) {
              $node_ids = $this->migrateLookup->lookup($node_plugin_definition, ['nid' => $node_data['nid']]);
              if (!empty($node_ids)) {
                if ($node_menu_link = $this->getMenuLinkFromNode(reset($node_ids)['nid'], $node_data['language'])) {
                  $menu_parent_mlid = $settings['menu_name'] . ':' . $node_menu_link;
                  break;
                }
              }
            }
          }
        }
      }
    }

    if (($hidden_option || $status_option) && is_array($settings)) {
      $original_block = $this->entityTypeManager->getStorage('block')
        ->load('menu_block_' . $block_delta);
      $original_block_settings = $original_block->get('settings');

      // If there the node and block settings are the same, we don't need an override.
      if ((!isset($settings['follow']) || $original_block_settings['follow_parent'] == $settings['follow'])
        && (!isset($settings['level']) || $original_block_settings['level'] == $settings['level'])
        && (!isset($settings['depth']) || $original_block_settings['depth'] == $settings['depth'])
        && (!isset($settings['parent']) || $original_block_settings['parent'] == $settings['parent'])
        && (!isset($settings['expanded']) || $original_block_settings['expand'] == $settings['expanded'])) {
        $status_option = 0;
      }

      $menu_block_settings = [
        'hidden' => $hidden_option,
        'status' => $status_option,
        'delta' => 0,
        'menu_block_id' => 'menu_block_' . $block_delta,
        'follow' => (empty($settings['follow']) || $settings['follow'] == 'active') ? 0 : 1,
        'follow_parent' => empty($settings['follow']) ? 'active' : $settings['follow'],
        'level' => $settings['level'] ?: $original_block_settings['level'],
        'depth' => $settings['depth'] ?: $original_block_settings['depth'],
        'expand' => $settings['expanded'] ?: $original_block_settings['expand'],
        'parent' => $menu_parent_mlid,
        'suggestion' => $original_block_settings['suggestion'],
        'selected_menus' => $original_block_settings['selected_menus'],
      ];

      $row->setSourceProperty('menu_block_settings', $menu_block_settings);
    }

    return parent::prepareRow($row);
  }

  /**
   * Gets the menu link pattern from node.
   *
   * @param $nid
   *   The Node ID.
   * @param $langcode
   *   The Node language.
   *
   * @return bool|string
   *   String containing the D8 menu link pattern.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getMenuLinkFromNode($nid, $langcode) {
    if ($node = $this->entityTypeManager->getStorage('node')->load($nid)) {
      if ($node->get('field_menulink')->isEmpty() === FALSE) {
        return "menu_link_field:node_field_menulink_{$node->uuid()}_{$langcode}";
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['nid']['type'] = 'integer';
    $ids['nid']['alias'] = 'n';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'nid' => $this->t('Node ID'),
      'type' => $this->t('Type'),
      'title' => $this->t('Title'),
      'node_uid' => $this->t('Node authored by (uid)'),
      'revision_uid' => $this->t('Revision authored by (uid)'),
      'created' => $this->t('Created timestamp'),
      'changed' => $this->t('Modified timestamp'),
      'status' => $this->t('Published'),
      'promote' => $this->t('Promoted to front page'),
      'sticky' => $this->t('Sticky at top of lists'),
      'revision' => $this->t('Create new revision'),
      'language' => $this->t('Language (fr, en, ...)'),
      'tnid' => $this->t('The translation set id for this node'),
      'timestamp' => $this->t('The timestamp the latest revision of this node was created.'),
    ];
    return $fields;
  }

}
