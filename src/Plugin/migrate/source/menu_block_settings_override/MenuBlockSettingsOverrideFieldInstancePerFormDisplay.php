<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\menu_block_settings_override;

/**
 * The field instance per form display source class.
 *
 * @MigrateSource(
 *   id = "d7_menu_block_settings_override_field_instance_per_form_display",
 *   source_module = "menu_block_settings_override"
 * )
 */
class MenuBlockSettingsOverrideFieldInstancePerFormDisplay extends MenuBlockSettingsOverrideFieldInstance {

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'entity_type' => [
        'type' => 'string',
      ],
      'bundle' => [
        'type' => 'string',
      ],
      'field_name' => [
        'type' => 'string',
        'alias' => 'fci',
      ],
    ];
  }


}
