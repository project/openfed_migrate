<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\menu_link_content;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\menu_link_content\Plugin\migrate\source\MenuLink;
use Drupal\migrate\MigrateLookupInterface;
use Drupal\migrate\Plugin\MigrationDeriverTrait;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Row;
use Drupal\openfed_migrate\Helper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal menu link source from database.
 *
 * @MigrateSource(
 *   id = "menu_link",
 *   source_module = "menu"
 * )
 */
class OpenfedMenuLink extends MenuLink {

  use MigrationDeriverTrait;

  /**
   * The migration plugin manager service.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * The migrate lookup service.
   *
   * @var \Drupal\migrate\MigrateLookupInterface
   */
  protected $migrateLookup;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, MigrationPluginManagerInterface $migration_plugin_manager, MigrateLookupInterface $migrate_lookup) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->migrateLookup = $migrate_lookup;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.migration'),
      $container->get('migrate.lookup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $mlids = [];
    $menulink_fields = static::getSourcePlugin('alternative_menu_link');
    if ($menulink_fields) {
      foreach ($menulink_fields as $menulink_field) {
        $mlids[] = $menulink_field->getSourceProperty('mlid');
      }
    }

    $query = parent::query();

    if (!empty($mlids)) {
      $query->condition('ml.mlid', $mlids, 'NOT IN');
    }

    $condition = $query->orConditionGroup()
      ->condition('pl.link_path', '/admin/%', 'NOT LIKE')
      ->isNull('pl.link_path');

    // Remove any /admin paths.
    $query->condition($condition);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    Helper::alterMenuLinkRow($row);

    $link_path = $row->getSourceProperty('link_path');

    // Fixes link_path property for translated term links.
    $this->setLinkPathPropertyForTermTranslation($row, $link_path);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return parent::getIds() +
      [
        'menu_name' => [
          'type' => 'string',
          'alias' => 'ml'
        ],
      ];
  }

  /**
   * Sets the correct "link_path" property for a translated term link.
   *
   * @param \Drupal\migrate\Row $row
   *   The current row to be migrated.
   * @param $link_path
   *   The source property link_path.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\migrate\MigrateException
   */
  protected function setLinkPathPropertyForTermTranslation(Row $row, $link_path) {
    // If the link path points to a term page.
    if (strpos($row->getSourceProperty('link_path'), 'taxonomy/term/') !== FALSE) {
      // Loads an Url object from the link_path property.
      $url = Url::fromUri('internal:/' . $link_path);
      // Gets the term id from the link.
      $term_id = str_replace('taxonomy/term/', '', $row->getSourceProperty('link_path'));
      // We try to load the term.
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($term_id);
      // If the Url is not routed or the term was
      // not found in D8 database we try to get
      // the path from the original term ID.
      if ($url->isRouted() === FALSE || $term === NULL) {
        // Migrate map translation table names.
        $migration_plugin_definitions = [
          'd7_taxonomy_term_translation',
          'upgrade_d7_taxonomy_term_translation',
          'd7_taxonomy_term_localized_translation',
          'upgrade_d7_taxonomy_term_localized_translation',
        ];
        // We do a lookup through the migration map
        // table in order to get the destination term ID
        // and if found, then we update the link_path property
        // with the original term link path.
        foreach ($migration_plugin_definitions as $plugin_definition) {
          if ($this->migrationPluginManager->hasDefinition($plugin_definition)) {
            $term_ids = $this->migrateLookup->lookup($plugin_definition, ['tid' => $term_id]);
            if (!empty($term_ids)) {
              $new_path = str_replace($term_id, reset($term_ids)['tid'], $row->getSourceProperty('link_path'));
              $row->setSourceProperty('link_path', $new_path);
              break;
            }
          }
        }
      }
    }
  }

}
