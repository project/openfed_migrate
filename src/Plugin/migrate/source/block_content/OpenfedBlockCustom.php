<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\block_content;

use Drupal\block_content\Plugin\migrate\source\d7\BlockCustom;
use Drupal\migrate\Row;

/**
 * Drupal 7 custom block from database.
 *
 * @MigrateSource(
 *   id = "d7_block_custom",
 *   source_module = "block"
 * )
 */
class OpenfedBlockCustom extends BlockCustom {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // It seems that comments in textarea's don't work well with CKeditor
    // if they have a new line.
    $body = $row->getSourceProperty('body');

    $body = preg_replace('(<!--[\s]*)', '<!--', $body);
    $body = preg_replace('([\s]*-->)', '-->', $body);

    $row->setSourceProperty('format', 'flexible_html');
    $row->setSourceProperty('body', $body);

    return parent::prepareRow($row);
  }

}
