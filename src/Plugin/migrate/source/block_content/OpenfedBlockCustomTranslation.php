<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\block_content;

use Drupal\block_content\Plugin\migrate\source\d7\BlockCustomTranslation;
use Drupal\migrate\Row;

/**
 * Gets Drupal 7 custom block translation from database.
 *
 * @MigrateSource(
 *   id = "d7_block_custom_translation",
 *   source_module = "i18n_block"
 * )
 */
class OpenfedBlockCustomTranslation extends BlockCustomTranslation {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // It seems that comments in textarea's don't work well with CKeditor
    // if they have a new line.
    $body = $row->getSourceProperty('body');

    $body = preg_replace('(<!--[\s]*)', '<!--', $body);
    $body = preg_replace('([\s]*-->)', '-->', $body);

    $row->setSourceProperty('format', 'flexible_html');
    $row->setSourceProperty('body', $body);
    $row->setSourceProperty('body_translated', $body);

    return parent::prepareRow($row);
  }

}
