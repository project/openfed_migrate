<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\language;

use Drupal\language\Plugin\migrate\source\d7\LanguageContentSettings;
use Drupal\migrate\Row;

/**
 * Drupal multilingual node settings from database.
 *
 * @MigrateSource(
 *   id = "d7_language_content_settings",
 *   source_module = "locale"
 * )
 */
class OpenfedLanguageContentSettings extends LanguageContentSettings {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    // Due to some migrate errors triggered by the default Openfed
    // configuration, we may need to check if language_content_settings entity
    // already exists to avoid duplicates.
    $entity_id = $row->getSourceProperty('constants/target_type') . '.' . $row->getSourceProperty('type');
    $language_content_settings_entity = \Drupal::entityTypeManager()
      ->getStorage('language_content_settings')
      ->load($entity_id);
    if ($language_content_settings_entity) {
      // If the entity already exists, we should remove it to avoid getting
      // errors. An entity exists, for example, if there is an Openfed default
      // node type (ie: page) that has the same name in the D7 database.
      // TODO: check if it's better to return false and ignore the row.
      $language_content_settings_entity->delete();
    }

    return parent::prepareRow($row);
  }

}
