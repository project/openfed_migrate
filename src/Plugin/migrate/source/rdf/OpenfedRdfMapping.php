<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\rdf;

use Drupal\rdf\Plugin\migrate\source\d7\RdfMapping;

/**
 * Drupal 7 rdf source from database.
 *
 * @MigrateSource(
 *   id = "d7_rdf_mapping",
 *   source_module = "rdf"
 * )
 */
class OpenfedRdfMapping extends RdfMapping {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // Check for entity types.
    // We need a subquery because otherwise there are conflicts with migrate
    // query updates.
    $database = \Drupal::database();
    $subquery = $database->select('node_type', 'nt')->fields('nt', ['type']);

    $query->condition('r.bundle', $subquery, 'IN');

    return $query;
  }

}
