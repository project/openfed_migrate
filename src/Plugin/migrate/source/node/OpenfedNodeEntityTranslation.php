<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\node;

use Drupal\node\Plugin\migrate\source\d7\NodeEntityTranslation;

/**
 * This class has the solo purpose to override getFieldValues() function called
 * by NodeEntityTranslation::prepareRow() in order to change the text filter
 * format passed to Openfed8 content.
 *
 * @MigrateSource(
 *   id = "d7_node_entity_translation",
 *   source_module = "entity_translation"
 * )
 */
class OpenfedNodeEntityTranslation extends NodeEntityTranslation {

  /**
   * This is an override to FieldEntity::getFieldValues() in order to change
   * the text filter format that is going to be set on Openfed 8. Openfed 8, by
   * default, has only one rich text formatter, called flexible_html.
   *
   * {@inheritdoc}
   */
  protected function getFieldValues($entity_type, $field, $entity_id, $revision_id = NULL, $language = NULL) {
    $table = (isset($revision_id) ? 'field_revision_' : 'field_data_') . $field;
    $query = $this->select($table, 't')
      ->fields('t')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->condition('deleted', 0);
    if (isset($revision_id)) {
      $query->condition('revision_id', $revision_id);
    }
    // Add 'language' as a query condition if it has been defined by Entity
    // Translation.
    if ($language) {
      $query->condition('language', $language);
    }
    $values = [];
    foreach ($query->execute() as $row) {
      foreach ($row as $key => $value) {
        $delta = $row['delta'];
        if (strpos($key, $field) === 0) {
          $column = substr($key, strlen($field) + 1);
          // The custom code, which overrides default function.
          if ($column == 'format') {
            $value = 'flexible_html';
          } else if ($column == 'value') {
            // It seems that comments in textareas don't work well with ckeditor
            // if they have a new line.
            $value = preg_replace('(<!--[\s]*)', '<!--', $value);
            $value = preg_replace('([\s]*-->)', '-->', $value);
          }
          $values[$delta][$column] = $value;
        }
      }
    }
    return $values;
  }

}
