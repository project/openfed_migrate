<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\weight;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal weight field items source from database.
 *
 * @MigrateSource(
 *   id = "d7_weight_field_items",
 *   source_module = "weight"
 * )
 */
class WeightFieldItems extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // We load the weights from D7 table.
    $query = $this->select('weight_weights', 'ww')->fields('ww');

    // We do a join to get all the nodes in the above conditions.
    $query->join('node', 'n', 'ww.entity_id = n.nid');
    $query->fields('n', ['type', 'nid', 'language', 'tnid']);

    // If the content_translation module is enabled, get the source langcode
    // to fill the content_translation_source field.
    if ($this->getModuleHandler()->moduleExists('content_translation')) {
      $query->leftJoin('node', 'nt', 'n.tnid = nt.nid');
      $query->addField('nt', 'language', 'source_langcode');
    }
    $this->handleTranslations($query);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'nid' => [
        'type' => 'integer',
        'alias' => 'n'
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'nid' => $this->t('Node ID'),
      'type' => $this->t('The Node type'),
      'language' => $this->t('Language (fr, en, ...)'),
      'tnid' => $this->t('The translation set id for this node'),
      'weight' => $this->t('The weight value.')
    ];
  }

  /**
   * Adapt our query for translations.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The generated query.
   */
  protected function handleTranslations(SelectInterface $query) {
    // Check whether or not we want translations.
    if (!isset($this->migration->getDestinationConfiguration()['translations'])) {
      // No translations: Yield untranslated nodes, or default translations.
      $query->where('n.tnid = 0 OR n.tnid = n.nid');
    }
    else {
      // Translations: Yield only non-default translations.
      $query->where('n.tnid <> 0 AND n.tnid <> n.nid');
    }
  }

}
