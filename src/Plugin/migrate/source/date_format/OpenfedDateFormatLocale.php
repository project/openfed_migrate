<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\date_format;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 date_format from database.
 *
 * @MigrateSource(
 *   id = "d7_date_formats_locale",
 *   source_module = "system",
 * )
 */
class OpenfedDateFormatLocale extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('date_format_locale', 'l');
    $query->fields('l', ['type', 'format', 'language']);
    $query->join('date_format_type', 't', "t.type = l.type");
    $query->fields('t', ['title']);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'type' => $this->t('Type'),
      'format' => $this->t('Format'),
      'title' => $this->t('Title'),
      'language' => $this->t('Language'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'type' => [
        'type' => 'string',
        'alias' => 't',
      ],
      'language' => [
        'type' => 'string',
        'alias' => 'l',
      ],
    ];
  }

}
