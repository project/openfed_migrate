<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\date_format;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 date_format from database.
 *
 * @MigrateSource(
 *   id = "d7_date_format",
 *   source_module = "system",
 * )
 */
class OpenfedDateFormat extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('date_format_type', 't');
    $query->fields('t', ['type', 'title']);
    $query->join('variable', 'v', "CONCAT('date_format_', t.type) = v.name");
    $query->addField('v', 'value', 'format');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'type' => $this->t('Type'),
      'format' => $this->t('Format'),
      'title' => $this->t('Title'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'type' => [
        'type' => 'string',
        'alias' => 't',
      ],
    ];
  }

}
