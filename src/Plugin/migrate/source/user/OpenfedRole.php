<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\user;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\user\Entity\Role;
use Drupal\user\Plugin\migrate\source\d7\Role as MigrationRole;

/**
 * Drupal 7 role source from database.
 *
 * @MigrateSource(
 *   id = "d7_user_role",
 *   source_module = "user"
 * )
 */
class OpenfedRole extends MigrationRole {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // We keep the same code from parent::prepareRow().
    $permissions = $this->select('role_permission', 'rp')
      ->fields('rp', ['permission'])
      ->condition('rid', $row->getSourceProperty('rid'))
      ->execute()
      ->fetchCol();

    // But we will merge permissions from Drupal7 Content Manager with Openfed8
    // roles.
    $machine_name = preg_replace('@[^a-z0-9-]+@','_', strtolower($row->getSourceProperty('name')));
    // We need to map roles to Openfed8 ones.
    switch($machine_name) {
      case 'authenticated_user':
        $machine_name = 'authenticated';
        break;
      case 'anonymous_user':
        $machine_name = 'anonymous';
        break;
      case 'content_manager':
        $machine_name = 'content_editor';
        break;
    }
    $role = Role::load($machine_name);
    // If the role exists already in Openfed8, we get it and update the
    // permissions.
    if ($role) {
      $existing_permissions = $role->getPermissions();
      $permissions = array_unique(array_merge($existing_permissions, $permissions));
      sort($permissions);
    }

    $row->setSourceProperty('permissions', $permissions);

    return DrupalSqlBase::prepareRow($row);
  }


}
