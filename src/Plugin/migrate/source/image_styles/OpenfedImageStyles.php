<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\image_styles;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ImageToolkit\ImageToolkitManager;
use Drupal\Core\State\StateInterface;
use Drupal\image\ImageEffectManager;
use Drupal\image\Plugin\migrate\source\d7\ImageStyles;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal image styles source from database.
 *
 * @MigrateSource(
 *   id = "d7_image_styles",
 *   source_module = "image"
 * )
 */
class OpenfedImageStyles extends ImageStyles {

  /**
   * The image effects plugins.
   *
   * @var array
   */
  protected $imageEffectsPlugins;

  /**
   * The allowed extensions from the default image toolkit.
   *
   * @var array
   */
  protected $allowedExtensions;

  /**
   * OpenfedImageStyles constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\image\ImageEffectManager $image_effect_manager
   *   The image effect manager.
   * @param \Drupal\Core\ImageToolkit\ImageToolkitManager $image_toolkit_manager
   *   The image toolkit manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, ImageEffectManager $image_effect_manager, ImageToolkitManager $image_toolkit_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
    $this->imageEffectsPlugins = $image_effect_manager->getDefinitions();
    $this->allowedExtensions = $image_toolkit_manager->getDefaultToolkit()->getSupportedExtensions();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.image.effect'),
      $container->get('image.toolkit.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $effects = $this->getEffects($row);

    // Nothing to do here.
    if (empty($effects)) {
      return DrupalSqlBase::prepareRow($row);
    }

    // This is a keyed array, by D7 image effects, old_name => new_name.
    $effects_mapping = [
      'coloractions_convert' => 'image_convert',
      'canvasactions_definecanvas' => 'image_effects_set_canvas',
    ];

    foreach ($effects as $key => &$effect) {
      $target_effect = $effects_mapping[$effect['name']] ?? [];

      // No target, no need to convert.
      if (empty($target_effect)) {
        continue;
      }

      // Check if the plugin exist.
      if (!isset($this->imageEffectsPlugins[$target_effect])) {
        throw new MigrateSkipRowException($this->t('Cannot map image effect plugin %old_plugin to %new_plugin, the %new_plugin does not exist.', [
          '%old_plugin' => $effect['name'],
          '%new_plugin' => $target_effect,
        ]));
      }

      // Set the correct data.
      if ($effect['name'] === 'coloractions_convert') {
        $this->setDataColorActionsConvert($effect);
      }

      // Set the new name.
      $effect['name'] = $target_effect;
    }

    $row->setSourceProperty('effects', $effects);

    // The parent method will override what we've done so use the base.
    return DrupalSqlBase::prepareRow($row);
  }

  /**
   * Set the data array for the 'coloractions_convert' plugin.
   *
   * Example of the conversion that is being done:
   *
   * $coloractions_convert = [
   *  'format' => 'image/jpeg',
   *  'quality => '75'
   * ];
   *
   * $image_convert = [
   *   'extension' => 'jpeg',
   * ];
   *
   * There is no substitution for quality at the moment.
   *
   * @param array $effect
   *   The effect that's being processed.
   *
   * @throws \Drupal\migrate\MigrateSkipRowException
   */
  protected function setDataColorActionsConvert(array &$effect) {
    $format = $effect['data']['format'] ?? '';

    // Nothing to change.
    if ($format === '') {
      return;
    }

    // Alter the format to an extension.
    $format = str_replace('image/', '', $format);

    // Check if the extension is allowed.
    if (!in_array($format, $this->allowedExtensions, TRUE)) {
      throw new MigrateSkipRowException($this->t('Cannot convert image effect %effect, extension %extension is not allowed.', [
        '%effect' => $effect['name'],
        '%format' => $format,
      ]));
    }

    $effect['data'] = [
      'extension' => $format,
    ];
  }

  /**
   * Get the image effects from the source
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   *
   * @return array
   *   The image effects data or empty.
   */
  protected function getEffects(Row $row) {
    $effects = [];

    $results = $this->select('image_effects', 'ie')
      ->fields('ie')
      ->condition('isid', $row->getSourceProperty('isid'))
      ->execute();

    foreach ($results as $key => $result) {
      $result['data'] = unserialize($result['data']);
      $effects[$key] = $result;
    }

    return $effects;
  }

}
