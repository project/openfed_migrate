<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source;

use Drupal\filter\Plugin\migrate\source\d7\FilterFormat;

/**
 * Perform a query alter in order to exclude unnecessary Openfed7 filters.
 *
 * @MigrateSource(
 *   id = "d7_filter_format",
 *   source_module = "filter"
 * )
 */
class OpenfedFilterFormat extends FilterFormat {

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('filter_format', 'f')
      ->fields('f')
      // We can ignore display suite text format since it doesn't seem to exist
      // in D8.
      ->condition('format', 'ds_code', '!=')
      ->condition('format', 'filtered_html', '!=')
      ->condition('format', 'full_html', '!=');
  }

}
