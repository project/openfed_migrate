<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\redirect;

use Drupal\migrate\Row;
use Drupal\redirect\Plugin\migrate\source\d7\PathRedirect;

/**
 * Drupal 7 path redirect source from database.
 *
 * @MigrateSource(
 *   id = "openfed_d7_path_redirect",
 *   source_module = "redirect"
 * )
 */
class OpenfedPathRedirect extends PathRedirect {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    // In D8 is not possible to disable redirects like in D7. As such, we should
    // not import disabled redirects, otherwise we'll get wrong
    // redirects/alias/menu links.
    // After checking some D7 sites, it was confirmed that disabled redirects
    // (status=0) are not being used.
    $query->condition('status','1');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);
    // Because d7_path_redirect plugin doesn't check if
    // the variable redirect_default_status_code has
    // actually a value, we need to set a default one
    // based on the default value in D7/D8.
    // @see redirect.settings.yml
    $current_status_code = $row->getSourceProperty('status_code');
    if ($current_status_code == FALSE) {
      $row->setSourceProperty('status_code', 301);
    }
    return $result;
  }

}
