<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\field_group;

use Drupal\field_group_migrate\Plugin\migrate\source\d7\FieldGroup;
use Drupal\migrate\Row;

/**
 * Drupal 7 field_group source.
 *
 * @MigrateSource(
 *   id = "d7_field_group",
 *   source_module = "field_group",
 *   destination_module = "field_group"
 * )
 */
class OpenfedFieldGroup extends FieldGroup {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Change bean entity_type to bean.
    if ($row->getSourceProperty('entity_type') == 'bean') {
      $row->setSourceProperty('entity_type', 'block_content');
    }

    return parent::prepareRow($row);
  }

}
