<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\file;

use Drupal\file\Plugin\migrate\source\d7\File;
use Drupal\migrate\Row;
use Drupal\openfed_migrate\Helper;

/**
 * Drupal 7 file source from database.
 *
 * @MigrateSource(
 *   id = "d7_file",
 *   source_module = "file"
 * )
 */
class OpenfedFile extends File {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    // Get Field API field values.
    $fid = $row->getSourceProperty('fid');
    $bundle = $row->getSourceProperty('type');
    $fields = Helper::getFields('file', $bundle);
    foreach ($fields as $field_name => $field) {
      $field_value = Helper::getFieldValues('file', $field_name, $fid);
      $row->setSourceProperty($field_name, $field_value);
    }

    return parent::prepareRow($row);
  }

}
