<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\bean;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;

/**
 * Gets Drupal 7 bean translation from database.
 * This will act on Beans translated using entity translation.
 *
 * @MigrateSource(
 *   id = "d7_bean_entity_translation",
 *   source_module = "entity_translation"
 * )
 */
class BeanEntityTranslation extends FieldableEntity {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('entity_translation', 'et')
      ->fields('et')
      ->fields('b', [
        'title',
        'type',
        'label',
        'delta',
        'vid',
        'bid',
      ])
      ->condition('b.type', $this->configuration['bean_type'], '=')
      ->condition('et.entity_type', 'bean');

    $query->innerJoin('bean', 'b', 'b.bid = et.entity_id');
    //$query->innerJoin('bean_revision', 'br', 'br.vid = et.revision_id');

    // Add in the translation for the property.
    //$query->innerJoin('locales_target', 'lt', 'lt.lid = i18n.lid');

    // Join block table to get the unique block id.
    $default_theme = $this->variableGet('theme_default', 'Garland');
    $query->leftJoin('block', 'block', 'block.delta=b.delta');
    $query->addField('block', 'bid', 'unique_bid');
    $query->condition('block.theme', $default_theme, 'LIKE');
    $query->condition('block.module', 'bean');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $bid = $row->getSourceProperty('entity_id');
    $vid = $row->getSourceProperty('vid');
    $type = $row->getSourceProperty('type');
    $language = $row->getSourceProperty('language');

    // Get Field API field values.
    foreach ($this->getFields('bean', $type) as $field_name => $field) {
      // Ensure we're using the right language if the entity is translatable.
      $field_language = $field['translatable'] ? $language : NULL;
      $row->setSourceProperty($field_name, $this->getFieldValues('bean', $field_name, $bid, $vid, $field_language));
    }

    return parent::prepareRow($row);
  }

  /**
   * This is an override to FieldEntity::getFieldValues() in order to change
   * the text filter format that is going to be set on Openfed 8. Openfed 8, by
   * default, has only one rich text formatter, called flexible_html.
   *
   * {@inheritdoc}
   */
  protected function getFieldValues($entity_type, $field, $entity_id, $revision_id = NULL, $language = NULL) {
    $table = (isset($revision_id) ? 'field_revision_' : 'field_data_') . $field;
    $query = $this->select($table, 't')
      ->fields('t')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->condition('deleted', 0);
    if (isset($revision_id)) {
      $query->condition('revision_id', $revision_id);
    }
    // Add 'language' as a query condition if it has been defined by Entity
    // Translation.
    if ($language) {
      $query->condition('language', $language);
    }
    $values = [];
    foreach ($query->execute() as $row) {
      foreach ($row as $key => $value) {
        $delta = $row['delta'];
        if (strpos($key, $field) === 0) {
          $column = substr($key, strlen($field) + 1);
          // The custom code, which overrides default function.
          if ($column == 'format') {
            $value = 'flexible_html';
          } else if ($column == 'value') {
            // It seems that comments in textareas don't work well with ckeditor
            // if they have a new line.
            $value = preg_replace('(<!--[\s]*)', '<!--', $value);
            $value = preg_replace('([\s]*-->)', '-->', $value);
          }
          $values[$delta][$column] = $value;
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'bid' => $this->t('The block numeric identifier.'),
      'format' => $this->t('Input format of the custom block/box content.'),
      'lid' => $this->t('i18n_string table id'),
      'language' => $this->t('Language for this field.'),
      'property' => $this->t('Block property'),
      'translation' => $this->t('The translation of the value of "property".'),
      'title' => $this->t('Block title.'),
      'title_translated' => $this->t('Block title translation.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'entity_id' => [
        'type' => 'integer',
        'alias' => 'et',
      ],
      'language' => [
        'type' => 'string',
        'alias' => 'et',
      ],
    ];
  }

}
