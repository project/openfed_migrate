<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\bean;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\openfed_migrate\Helper;

/**
 * Drupal 7 Bean Translation settings. All beans should be translatable in
 * Drupal 8 installations.
 *
 * @MigrateSource(
 *   id = "d7_bean_translation_settings",
 *   source_module = "bean"
 * )
 */
class BeanTranslationSettings extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // This query will allow us to avoid migrating beans without content which
    // usually means that this bean type is corrupt (probably due to features in
    // Drupal 7).
    $query = $this->select('bean', 'b');
    $query->leftJoin('bean_type', 'bt', 'bt.name = b.type');
    $query->addField('bt', 'label');
    $query->addField('bt', 'description');
    $query->addField('b', 'type', 'name');
    $query->groupBy('bt.label');
    $query->groupBy('bt.description');
    $query->groupBy('b.type');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'name' => $this->t('The bean ID'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['name']['type'] = 'string';
    return $ids;
  }


}
