<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\bean;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;
use Drupal\openfed_migrate\Helper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal 7 Bean source from database.
 *
 * @MigrateSource(
 *   id = "d7_bean_item",
 *   source_module = "bean"
 * )
 */
class BeanItem extends FieldableEntity {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityManagerInterface $entity_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_manager);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Select bean in its last revision.
    $query = $this->select('bean_revision', 'br')
      ->fields('b', [
        'bid',
        'title',
        'label',
        'type',
        'uid',
        'created',
        'changed',
      ])
      ->fields('br', [
        'vid',
        'title',
        'delta',
        'log',
      ]);
    $query->addField('b', 'uid', 'block_uid');
    $query->addField('br', 'uid', 'revision_uid');
    $query->innerJoin('bean', 'b', 'b.vid = br.vid');


    if (isset($this->configuration['bean_type'])) {
      $query->condition('b.type', $this->configuration['bean_type']);
    }

    // Add extra validation for existing bundles.
    // Left join bean types to later check if they exist. We additionally add a
    // check to see if there is bean content because in some cases bean_type
    // table is empty and entity type info is in features, allowing content to
    // be added.
    $query->leftJoin('bean_type', 'bean_type', "b.type = bean_type.name");
    $query->leftJoin('bean', 'bean', "b.type = bean.type");
    $query->condition('bean.type', NULL, 'IS NOT NULL');
    $query->distinct();
    $query->orderBy('b.bid');

    // Join block table to get the unique block id.
    $default_theme = $this->variableGet('theme_default', 'Garland');
    $query->leftJoin('block', 'block', 'block.delta=b.delta');
    $query->addField('block', 'bid', 'unique_bid');
    $query->condition('block.theme', $default_theme, 'LIKE');
    $query->condition('block.module', 'bean');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    // Add extra validation for existing bundles.
    if (!Helper::sourcePrepareRowBundleValidator($row, 'type')) {
      return FALSE;
    }
    // Get Field API field values.
    foreach (array_keys($this->getFields('bean', $row->getSourceProperty('type'))) as $field) {
      $bid = $row->getSourceProperty('bid');
      $vid = $row->getSourceProperty('vid');
      $row->setSourceProperty($field, $this->getFieldValues('bean', $field, $bid, $vid));
    }
    return parent::prepareRow($row);
  }

  /**
   * This is an override to FieldEntity::getFieldValues() in order to change
   * the text filter format that is going to be set on Openfed 8. Openfed 8, by
   * default, has only one rich text formatter, called flexible_html.
   *
   * {@inheritdoc}
   */
  protected function getFieldValues($entity_type, $field, $entity_id, $revision_id = NULL, $language = NULL) {
    $table = (isset($revision_id) ? 'field_revision_' : 'field_data_') . $field;
    $query = $this->select($table, 't')
      ->fields('t')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->condition('deleted', 0);
    if (isset($revision_id)) {
      $query->condition('revision_id', $revision_id);
    }
    // Add 'language' as a query condition if it has been defined by Entity
    // Translation.
    if ($language) {
      $query->condition('language', $language);
    }
    $values = [];
    foreach ($query->execute() as $row) {
      foreach ($row as $key => $value) {
        $delta = $row['delta'];
        if (strpos($key, $field) === 0) {
          $column = substr($key, strlen($field) + 1);
          // The custom code, which overrides default function.
          if ($column == 'format') {
            $value = 'flexible_html';
          } else if ($column == 'value') {
            // It seems that comments in textareas don't work well with ckeditor
            // if they have a new line.
            $value = preg_replace('(<!--[\s]*)', '<!--', $value);
            $value = preg_replace('([\s]*-->)', '-->', $value);
          }
          $values[$delta][$column] = $value;
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'bid' => $this->t('Block ID'),
      'type' => $this->t('Type'),
      'title' => $this->t('Title'),
      'uid' => $this->t('Authored by (uid)'),
      'revision_uid' => $this->t('Revision authored by (uid)'),
      'created' => $this->t('Created timestamp'),
      'changed' => $this->t('Modified timestamp'),
      'revision' => $this->t('Create new revision'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['bid']['type'] = 'integer';
    $ids['bid']['alias'] = 'b';
    return $ids;
  }

}
