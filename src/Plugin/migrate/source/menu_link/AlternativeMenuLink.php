<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\menu_link;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\menu_link_content\Plugin\migrate\source\MenuLink;
use Drupal\migrate\Plugin\MigrationDeriverTrait;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal menu link source from database.
 *
 * @MigrateSource(
 *   id = "alternative_menu_link",
 *   source_module = "menu"
 * )
 */
class AlternativeMenuLink extends MenuLink {

  use MigrationDeriverTrait;

  /**
   * Statis Menu Link Fields as migrated by d7_menu_link_field_instances.
   *
   * @var \Drupal\openfed_migrate\Plugin\migrate\source\menu_link\MenuLinkFieldInstances
   */
  private $menulink_fields;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, $menulink_fields) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $this->entityTypeManager = $entity_type_manager);
    $this->menulink_fields = $menulink_fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      static::getSourcePlugin('d7_menu_link_field_instances')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // We only care about nodes since other entities won't have menu_link field.
    // If other entities are present in the menu, they will be migrated with
    // OpenfedMenuLink.
    $query->condition('ml.router_path', 'node/%', '=');

    // We need to get the bundles. (d7_menu_link_field_instances)
    // We need to get the menus enabled. (field_menulink settings, based on d7_menu_link_field_instances)
    $menulink_fields = $this->getMenuLinkFieldInstances();

    // If there are no bundles with menus enabled, then these nodes don't have a
    // menu set using their node edit form. If that's the case, these nodes will
    // be imported with OpenfedMenuLink.
    if (empty($menulink_fields['menus_enabled']) || empty($menulink_fields['bundles'])) {
      return $query->range(0, 0);
    }

    // We need a condition for the main menu since in D7 it was called
    // "main-menu" and in D8 is called just "main".
    if (isset($menulink_fields['menus_enabled']['main'])) {
      $menulink_fields['menus_enabled']['main'] = 'main-menu';
    }

    $query->join('node', 'n', "ml.link_path LIKE CONCAT('node/', n.nid)");
    $query->fields('n', ['nid', 'language', 'type']);

    // We don't need to check if there are menus enabled or bundles. That should
    // be handled before this line and the query should have already been
    // returned.
    $query->condition('ml.menu_name', $menulink_fields['menus_enabled'], 'IN');
    $query->condition('n.type', $menulink_fields['bundles'], 'IN');

    $query->leftJoin('node', 'pn', "pl.link_path LIKE CONCAT('node/', pn.nid)");
    $query->addField('pn', 'nid', 'pnid');
    $query->addField('pn', 'language', 'planguage');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $description = $row->getSourceProperty('description');
    if (is_null($description)) {
      $row->setSourceProperty('description', '');
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return parent::getIds() +
      [
        'menu_name' => [
          'type' => 'string',
          'alias' => 'ml'
        ],
      ];
  }

  /**
   * Get bundles and menus of migrated d7_menu_link_field_instances.
   *
   * @return array
   */
  private function getMenuLinkFieldInstances() {
    $bundles = [];
    $menus_enabled = [];
    if ($this->menulink_fields) {
      foreach ($this->menulink_fields as $menulink_field) {
        $bundles[] = $menulink_field->getSourceProperty('bundle');
        $menus_enabled += array_filter($menulink_field->getSourceProperty('settings/available_menus'));
      }
    }

    return [
      'bundles' => $bundles,
      'menus_enabled' => $menus_enabled,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $rows = $this->query()->execute()->fetchAll();

    foreach ($rows as $key => $row) {
      // If the menu link is disabled we try to find
      // menu links that point to the same node id
      // and if found we remove the disabled ones.
      // In the end it will remain the enabled one.
      if ($row['hidden']) {
        $search_duplicate_keys = array_keys(array_column($rows, 'nid'), $row['nid']);
        if (count($search_duplicate_keys) > 1) {
          foreach ($search_duplicate_keys as $duplicate_key) {
            if ($rows[$duplicate_key]['mlid'] !== $row['mlid']) {
              if ($rows[$duplicate_key]['hidden'] == FALSE) {
                unset($rows[$key]);
                break;
              }
            }
          }
        }
      } else {
        // Since we are migrating links to field_menulink here, and it has a
        // cardinality of 1, we should avoid trying to migrate several links to
        // the same node here. Here we should migrate only one link and let
        // d7_meny_links take care of the rest.
        $search_duplicate_keys = array_keys(array_column($rows, 'nid'), $row['nid']);
        if (count($search_duplicate_keys) > 1) {
          foreach ($search_duplicate_keys as $duplicate_key) {
            if ($rows[$duplicate_key]['mlid'] !== $row['mlid']) {
              unset($rows[$key]);
              break;

            }
          }
        }
      }
    }

    return new \ArrayIterator($rows);
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    return $this->initializeIterator()->count();
  }

}
