<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\menu_link;

/**
 * The field instance per form display source class.
 *
 * @MigrateSource(
 *   id = "d7_menu_link_field_instance_per_form_display",
 *   source_module = "field"
 * )
 */
class MenuLinkFieldInstancePerFormDisplay extends MenuLinkFieldInstances {

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'entity_type' => [
        'type' => 'string',
      ],
      'bundle' => [
        'type' => 'string',
      ],
      'field_name' => [
        'type' => 'string',
        'alias' => 'fci',
      ],
    ];
  }


}
