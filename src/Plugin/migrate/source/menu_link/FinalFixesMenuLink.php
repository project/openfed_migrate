<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\menu_link;

use Drupal\migrate\Row;
use Drupal\openfed_migrate\Helper;
use Drupal\openfed_migrate\Plugin\migrate\source\menu_link_content\OpenfedMenuLink;


/**
 * Drupal menu link source from database.
 * These are important fixes that will help migrate links with special paths,
 * like "<nolink>", and it will help to migrate menu items with a language X
 * belonging to a parent in language Y.
 *
 * @MigrateSource(
 *   id = "final_fixes_menu_link",
 *   source_module = "menu"
 * )
 */
class FinalFixesMenuLink extends OpenfedMenuLink {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $mlids = [];
    $table_prefix = 'migrate_map_';

    // Migration dependencies will define on which tables should we look for links missing an import.
    $required = $this->migration->getMigrationDependencies()['required'];

    if ($required) {
      $d8_database = \Drupal::database();
      $query = '';
      foreach ($required as $key => $migration_process) {
        if ($key == 0) {
          $query = $d8_database->select($table_prefix.$migration_process, 'table'.$key);
          $query->fields('table' . $key, ['sourceid1']);
          $query->condition('table'.$key.'.destid1', NULL, 'IS NULL');
        } else {
          $query->join($table_prefix.$migration_process, 'table'.$key, 'table'.($key-1).'.sourceid1=table'.$key.'.sourceid1');
          $query->condition('table'.$key.'.destid1', NULL, 'IS NULL');
        }
      }
      $result = $query->execute()->fetchAll();
    }

    if ($result) {
      foreach ($result as $sourceid) {
        $mlids[] = $sourceid->sourceid1;
      }
    }

    $query = parent::query();
    if ($mlids) {
      $query->condition('ml.mlid', $mlids, 'IN');
    }

    $query->leftJoin('node', 'pn', "pl.link_path LIKE CONCAT('node/', pn.nid)");
    $query->addField('pn', 'nid', 'pnid');
    $query->addField('pn', 'language', 'planguage');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    Helper::alterMenuLinkRow($row);

    return parent::prepareRow($row);
  }

}
