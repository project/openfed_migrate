<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\menu;

use Drupal\migrate\Row;
use Drupal\system\Plugin\migrate\source\Menu;

/**
 * Override core Menu source from database in order to add language property.
 *
 * @MigrateSource(
 *   id = "menu",
 *   source_module = "menu"
 * )
 */
class OpenfedMenu extends Menu {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Langcode language mapping to look for.
    $langcode_mapping = [
      'fr' => 'fr',
      'french' => 'fr',
      'nl' => 'nl',
      'dutch' => 'nl',
      'de' => 'de',
      'deutch' => 'de',
      'deutsch' => 'de',
      'german' => 'de',
      'en' => 'en',
      'english' => 'en',
    ];

    // The strings where we will search for a language string.
    $menu_name = $row->getSourceProperty('menu_name');
    $title = $row->getSourceProperty('title');

    $haystack = $menu_name . ' ' . $title;
    $needles = array_keys($langcode_mapping);
    $langcode = 'en';
    foreach($needles as $needle) {
      preg_match('/[^a-z][ ._-]?('.$needle.')[^a-z][ ._-]?/', $haystack, $matches);
      if ($matches[1] == $needle) {
        $langcode = $langcode_mapping[$needle];
        break;
      }
    }
    $row->setSourceProperty('langcode', $langcode);

    return parent::prepareRow($row);
  }

}
