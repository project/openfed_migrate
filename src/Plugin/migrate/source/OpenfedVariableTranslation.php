<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source;

use Drupal\migrate_drupal\Plugin\migrate\source\d7\VariableTranslation;

/**
 * Gets Drupal variable_store source from database.
 * This override will change the variable values, if they are internal paths to
 * node pages, in order for them to have an initial slash.
 * If this is not used, after running d7_system_site_translation an error will
 * start to show due to incorrect variable translation without initial slash.
 *
 * @MigrateSource(
 *   id = "d7_variable_translation",
 *   source_module = "i18n_variable",
 * )
 */
class OpenfedVariableTranslation extends VariableTranslation {

  /**
   * {@inheritdoc}
   */
  protected function values() {
    // Let's use parent function and just change the previously generated output.
    $values = parent::values();

    foreach ($values as &$translatedVars) {
      foreach ($translatedVars as &$value) {
        if (strpos($value, 'node/') === 0) {
          $value = '/' . $value;
        }
      }
    }

    return $values;

  }

}
