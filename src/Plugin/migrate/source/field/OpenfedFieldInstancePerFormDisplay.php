<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\field;

use Drupal\field\Plugin\migrate\source\d7\FieldInstancePerFormDisplay;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Row;
use Drupal\openfed_migrate\Helper;

/**
 * The field instance per form display source class.
 *
 * @MigrateSource(
 *   id = "d7_field_instance_per_form_display",
 *   source_module = "field"
 * )
 */
class OpenfedFieldInstancePerFormDisplay extends FieldInstancePerFormDisplay {


  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // This fixes some issues that occur when comment module is disabled but
    // fields exist in Openfed 7 table (possibly due to features).
    if (!$this->moduleExists('comment')) {
      $query->condition('fci.entity_type', 'comment', 'NOT IN');
    }

    // This fixes some issues that occur when bean module is disabled but
    // fields exist in Openfed 7 table.
    if (!$this->moduleExists('bean')) {
      $query->condition('fci.entity_type', 'bean', 'NOT IN');
    }

    // Check for bean fields.
    $schema = $this->getDatabase()->schema();
    Helper::sourceQueryAlter($query, $schema);

    // Exclude D7 default file_entity fields since they were renamed in D8.
    Helper::ignoreFields($query);

    return $query;
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function prepareRow(Row $row) {
    // Change the old field widgets into a Drupal 8 matches. This may not give
    // the ideal widget but a similar one.
    $data = unserialize($row->getSourceProperty('data'));

    switch ($data['widget']['type']) {
      case 'entityreference_view_widget':
        $data['widget']['type'] = 'entity_browser_entity_reference';
        $data['widget']['module'] = 'entity_browser';
        // TODO: set the correct settings, when the views get migrated.
        $data['widget']['settings'] = [];
        break;

      case 'enfield_widget':
        $data['widget']['type'] = 'file_generic';
        $data['widget']['settings'] = [];
        break;

      case 'formatter_select':
        $data['widget']['type'] = 'formatter_field_formatter';
        $data['widget']['settings'] = [];
        break;

      case 'taxonomy_hs':
        $data['widget']['type'] = 'cshs';
        $data['widget']['settings'] = [];
        break;

      case 'date_select':
        if (isset($data['settings']['default_value2'])) {
          $data['widget']['type'] = 'daterange_datelist';
        }
        break;

      case 'date_popup':
        $data['widget']['type'] = 'date_default';

        if (isset($data['settings']['default_value2'])) {
          $data['widget']['type'] = 'daterange_default';
        }
        break;

      case 'image':
        $data['widget']['type'] = 'image_image';
        break;

      case 'emailwidget':
        $data['widget']['type'] = 'text_textfield';
        $data['widget']['module'] = 'core';
        break;

      case 'inline_entity_form':
        $data['widget']['type'] = 'inline_entity_form_complex';
	      break;

      case 'urlwidget':
        $data['widget']['type'] = 'link_default';
        $data['widget']['module'] = 'link';
	      break;

      case 'numberfield':
        $data['widget']['type'] = 'number';
        $data['widget']['module'] = 'core';
        break;

      case 'text_textfield':
        if (Helper::isMigrationFieldRowSimpleGmap($row->getSourceProperty('field_name'))) {
          $data['widget']['module'] = 'address';
          $data['widget']['type'] = 'address_default';
          $data['widget']['settings'] = [];
        }
        break;

      case 'geocoder':
        $data['widget']['module'] = 'geofield';
        $data['widget']['type'] = 'geofield_latlon';
        $data['widget']['settings'] = [];
        break;

      case 'location':
        $data['widget']['module'] = 'geolocation';
        $data['widget']['type'] = 'geolocation_leaflet';
        $data['widget']['settings'] = [];
        // TODO: see how to set third party settings.
//        $data['widget']['third_party_settings'] = [
//          'enable' => true,
//          'address_field' => $row->getSourceProperty('field_name') . '_ext',
//
//        ];
        break;
    }

    $row->setSourceProperty('data', serialize($data));

    // Fix issue with multiple instances of a text_long
    // when using different text_processing per instance.
    Helper::enableTextProcessingOnMultipleInstances($row);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $results = parent::initializeIterator();

    foreach ($results as $index => $result) {
      if ($result['type'] == 'location') {
        // Every location field type will be split in two. This is the alternative
        // in D8 to store coordinates (geolocation) and address (address).
        $results[$index]['type'] = 'geolocation';
        foreach ($result['instances'] as &$field_instance) {
          $field_instance['type'] = 'geolocation';
        }

        $new_field = $result;
        $new_field['id'] .= 'new';
        $new_field['field_id'] .= 'new';
        $new_field['field_name'] .= '_ext';
        $new_field['type'] = 'address';
        foreach ($new_field['instances'] as &$instance) {
          $instance['type'] = 'address';
        }
        $results[] = $new_field;
      }
    }

    return new \ArrayIterator($results);
  }

  /**
   * @param \Drupal\migrate\Plugin\MigrateIdMapInterface $idMap
   */
  public function setIdMap(MigrateIdMapInterface $idMap) {
    $this->idMap = $idMap;
  }

}
