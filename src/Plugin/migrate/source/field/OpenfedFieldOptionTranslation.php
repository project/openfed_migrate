<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\field;

use Drupal\field\Plugin\migrate\source\d7\Field;
use Drupal\migrate\Row;

/**
 * Gets field option label translations.
 * This class overrides the original FieldOptionTranslation just by altering
 * the query and avoiding sql errors.
 *
 * @MigrateSource(
 *   id = "d7_field_option_translation",
 *   source_module = "i18n_field"
 * )
 */
class OpenfedFieldOptionTranslation extends Field {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->leftJoin('i18n_string', 'i18n', 'i18n.type = fc.field_name');
    $query->innerJoin('locales_target', 'lt', 'lt.lid = i18n.lid');
    $query->condition('i18n.textgroup', 'field')
      ->condition('objectid', '#allowed_values');
    // Add all i18n and locales_target fields.

    // Actually, by adding all the fields, we get a sql error due to repeated
    // fields in the query. This custom query removes all the fields,
    // keeping the essential. It also remove fc.type because this has been added
    // in the parent query.
    $query->fields('lt', ['language', 'translation']);
    $query->addField('fci', 'bundle');
    $query->addField('i18n', 'lid', 'i18n_lid');
    $query->addField('i18n', 'type', 'i18n_type');
    $query->addField('i18n', 'property');
    $query->orderBy('i18n.lid');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row, $keep = TRUE) {
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'bundle' => $this->t('Entity bundle'),
      'lid' => $this->t('Source string ID'),
      'textgroup' => $this->t('A module defined group of translations'),
      'context' => $this->t('Full string ID'),
      'objectid' => $this->t('Object ID'),
      'property' => $this->t('Object property for this string'),
      'objectindex' => $this->t('Integer value of Object ID'),
      'format' => $this->t('The input format used by this string'),
      'translation' => $this->t('Translation of the option'),
      'language' => $this->t('Language code'),
      'plid' => $this->t('Parent lid'),
      'plural' => $this->t('Plural index number in case of plural strings'),
      'i18n_status' => $this->t('A boolean indicating whether this translation needs to be updated'),
    ];
    return parent::fields() + $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return parent::getIds() +
      [
        'language' => ['type' => 'string'],
        'property' => ['type' => 'string'],
      ];
  }

}
