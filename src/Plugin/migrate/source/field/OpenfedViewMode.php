<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\field;

use Drupal\field\Plugin\migrate\source\d7\ViewMode;
use Drupal\migrate\Row;

/**
 * The view mode source class.
 *
 * This override will skip comment view modes migration if the module is
 * disabled and it will migrate bean view modes as block content entity.
 *
 * @MigrateSource(
 *   id = "d7_view_mode",
 *   source_module = "field"
 * )
 */
class OpenfedViewMode extends ViewMode {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    
    // This fixes some issues that occur when comment module is disabled but
    // fields exist in Openfed 7 table (possibly due to features).
    if (!$this->moduleExists('comment')) {
      $query->condition('fci.entity_type', 'comment', 'NOT IN');
    }

    // This fixes some issues that occur when bean module is disabled but
    // fields exist in Openfed 7 table.
    if (!$this->moduleExists('bean')) {
      $query->condition('fci.entity_type', 'bean', 'NOT IN');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    //    if ($row->getSourceProperty('entity_type') == 'bean') {
    //      $row->setSourceProperty('entity_type', 'block_content');
    //    }

    parent::prepareRow($row);
  }

}
