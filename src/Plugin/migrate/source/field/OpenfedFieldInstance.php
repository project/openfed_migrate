<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\field\Plugin\migrate\source\d7\FieldInstance;
use Drupal\migrate\MigrateMessage;
use Drupal\migrate\MigrateMessageInterface;
use Drupal\migrate\Plugin\MigrationDeriverTrait;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\openfed_migrate\Helper;

/**
 * Drupal 7 field instances source from database.
 *
 * @MigrateSource(
 *   id = "d7_field_instance",
 *   source_module = "field"
 * )
 */
class OpenfedFieldInstance extends FieldInstance {

  use MigrationDeriverTrait;

  /**
   * Migration message service.
   *
   * @var \Drupal\migrate\MigrateMessageInterface
   */
  protected $message;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, MigrateMessageInterface $message = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
    $this->message = $message ?: new MigrateMessage();
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    // This fixes some issues that occur when comment module is disabled but
    // fields exist in Openfed 7 table (possibly due to features).
    if (!$this->moduleExists('comment')) {
      $query->condition('fci.entity_type', 'comment', 'NOT IN');
    }

    // This fixes some issues that occur when bean module is disabled but
    // fields exist in Openfed 7 table.
    if (!$this->moduleExists('bean')) {
      $query->condition('fci.entity_type', 'bean', 'NOT IN');
    }

    // Check for entity types.
    $schema = $this->getDatabase()->schema();
    Helper::sourceQueryAlter($query, $schema);

    // Exclude D7 default file_entity fields since they were renamed in D8.
    Helper::ignoreFields($query);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    parent::prepareRow($row);

    if ($row->getSourceProperty('type') === 'datetime' || $row->getSourceProperty('type') === 'datestamp') {
      $data = unserialize($row->getSourceProperty('data'));
      if (!isset($data['default_value']) && isset($data['settings'])) {
        $defaults = [
          'default_date_type' => $data['settings']['default_value'],
          'default_date' => $data['settings']['default_value'],
        ];

        if (isset($data['settings']['default_value2'])) {
          if ($data['settings']['default_value2'] == 'same') {
            $defaults['default_end_date_type'] = $data['settings']['default_value'];
            $defaults['default_end_date'] = $data['settings']['default_value'];
          }
          else {
            $defaults['default_end_date_type'] = $data['settings']['default_value2'];
            $defaults['default_end_date'] = $data['settings']['default_value2'];
          }
        }

        $data['default_value'][] = $defaults;

        $row->setSourceProperty('data', serialize($data));
      }
    }

    // Translated vocabularies should have translatable fields in D8.
    if ($row->getSourceProperty('entity_type') == 'taxonomy_term' && $isTaxonomyFieldTranslatable = Helper::vocabularyIsTranslatable($row->getSourceProperty('bundle'))) {
      $instances = $row->getSourceProperty('instances');
      if (!empty($instances) && count($instances) > 0) {
        foreach ($instances as $key => $instance) {
          $instances[$key]['translatable'] = $isTaxonomyFieldTranslatable;
        }
        $row->setSourceProperty('instances', $instances);
        $row->setSourceProperty('translatable', $isTaxonomyFieldTranslatable) ;
      }
    }

    // Fix issue with multiple instances of a text_long
    // when using different text_processing per instance.
    Helper::enableTextProcessingOnMultipleInstances($row);

    // Fix formatter field settings.
    self::setFormatterFieldSettings($row);

    // Fix country field settings.
    self::setCountryFieldSettings($row);

    // Fix emfield field settings.
    self::setEmFieldSettings($row);

    // Fix address/geolocation field settings.
    self::setAddressSettings($row);

    return DrupalSqlBase::prepareRow($row);
  }

  /**
   * Set the formatter field array settings.
   *
   * Example of the conversion that is being done:
   *
   * $d7 = [
   *  'field_name' => 'field_frontpage_image_formatter',
   *  'user_register_form => FALSE
   * ];
   *
   * $d8 = [
   *   'field' => 'field_frontpage_image_formatter',
   * ];
   *
   * There is no substitution for user_register_form at the moment.
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   */
  protected static function setFormatterFieldSettings(Row $row) {
    // Only change formatters.
    if ($row->getSourceProperty('type') !== 'formatter') {
      return;
    }

    $data = $row->getSourceProperty('data') ?? [];

    // Nothing to change.
    if (empty($data)) {
      return;
    }

    $data = unserialize($data);

    $field_name = $data['settings']['field_name'] ?? '';

    // No field name found.
    if ($field_name === '') {
      return;
    }

    // Create the correct settings array.
    $data['settings'] = [
      'field' => $field_name,
    ];

    $row->setSourceProperty('data', serialize($data));
  }

  /**
   * Set the country field field settings.
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   */
  protected static function setCountryFieldSettings(Row $row) {
    if ($row->getSourceProperty('type') !== 'country') {
      return;
    }
    $data = $row->getSourceProperty('data') ?? [];

    // Nothing to change.
    if (empty($data)) {
      return;
    }

    $data = unserialize($data);

    // Set default value.
    $data['default_value'][0]['value'] = $data['default_value'][0]['iso2'];

    $row->setSourceProperty('data', serialize($data));
  }

  /**
   * Set the emfield field settings.
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   */
  protected static function setEmFieldSettings(Row $row) {
    if ($row->getSourceProperty('type') !== 'file') {
      return;
    }
    $data = $row->getSourceProperty('data') ?? [];

    // Nothing to change.
    if (empty($data)) {
      return;
    }

    $data = unserialize($data);

    $widget_type = $data['widget']['type'] ?? '';

    // Only change enfield_widget types.
    if ($widget_type !== 'enfield_widget') {
      return;
    }

    // Change the description so it reflects the default field settings in D8.
    $data['description'] = 'Fill the URL of this video (support YouTube, YouTube Playlist and Vimeo).';

    $row->setSourceProperty('data', serialize($data));
  }

  /**
   * Set the address field settings.
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   */
  protected function setAddressSettings(Row $row) {

    // Only alter for geolocation fields and fields that use simple gmap.
    if (!Helper::isMigrationFieldRowSimpleGmap($row->getSourceProperty('field_name')) && $row->getSourceProperty('type') !== 'geolocation') {
      return;
    }

    $field_name = $row->getSourceProperty('field_name') ?? '';

    // If we are handling geolocation or simple_gmap fields, we send a message
    // to warn the user about necessary manual actions to take after.
    $this->message->display(
      $this->t(
        'The field @field will need manual adjustments after the migration for formatter and widget.',
        [
          '@field' => $field_name,
        ]
      ),
      'alert'
    );

  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $results = parent::initializeIterator();

    foreach ($results as $index => $result) {
      if ($result['type'] == 'location') {
        // Every location field type will be split in two. This is the alternative
        // in D8 to store coordinates (geolocation) and address (address).
        $results[$index]['type'] = 'geolocation';
        foreach ($result['instances'] as &$field_instance) {
          $field_instance['type'] = 'geolocation';
        }

        $new_field = $result;
        $new_field['id'] .= 'new';
        $new_field['field_id'] .= 'new';
        $new_field['field_name'] .= '_ext';
        $new_field['type'] = 'addressfield';
        foreach ($new_field['instances'] as &$instance) {
          $instance['type'] = 'addressfield';
        }

        $results[] = $new_field;
      }
    }

    return new \ArrayIterator($results);
  }

}
