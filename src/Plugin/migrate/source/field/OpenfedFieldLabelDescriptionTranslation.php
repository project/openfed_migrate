<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\field;

use Drupal\field\Plugin\migrate\source\d7\FieldLabelDescriptionTranslation;

/**
 * Gets field label and description translations.
 *
 * @MigrateSource(
 *   id = "d7_field_instance_label_description_translation",
 *   source_module = "i18n_field"
 * )
 */
class OpenfedFieldLabelDescriptionTranslation extends FieldLabelDescriptionTranslation {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Get translations for field labels and descriptions.
    $query = parent::query();
    // This fixes some issues that occur when comment module is disabled but
    // fields exist in Openfed 7 table (possibly due to features).
    if (!$this->moduleExists('comment')) {
      $query->condition('fci.entity_type', 'comment', 'NOT IN');
    }

    return $query;
  }

}
