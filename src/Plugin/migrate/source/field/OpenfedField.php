<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\field;

use ArrayIterator;
use Drupal\field\Plugin\migrate\source\d7\Field;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\openfed_migrate\Helper;

/**
 * Drupal 7 field source from database.
 *
 * @MigrateSource(
 *   id = "d7_field",
 *   source_module = "field_sql_storage"
 * )
 */
class OpenfedField extends Field {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    // This fixes some issues that occur when comment module is disabled but
    // fields exist in Openfed 7 table (possibly due to features).
    if (!$this->moduleExists('comment')) {
      $query->condition('fci.entity_type', 'comment', 'NOT IN');
    }

    // This fixes some issues that occur when bean module is disabled but
    // fields exist in Openfed 7 table.
    if (!$this->moduleExists('bean')) {
      $query->condition('fci.entity_type', 'bean', 'NOT IN');
    }

    // Check for entity types.
    $schema = $this->getDatabase()->schema();
    Helper::sourceQueryAlter($query, $schema);

    // Exclude D7 default file_entity fields since they were renamed in D8.
    Helper::ignoreFields($query);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $results = $this->prepareQuery()->execute()->fetchAll();

    foreach ($results as $index => $result) {
      // Every location field type will be split in two. This is the alternative
      // in D8 to store coordinates (geolocation) and address (address).
      if ($result['type'] == 'location') {
        $results[$index]['type'] = 'geolocation';
        $results[$index]['module'] = 'geolocation';

        $new_field = $result;
        $new_field['id'] .= 'new';
        $new_field['field_name'] .= '_ext';
        $new_field['type'] = 'addressfield';
        $new_field['module'] = 'address';
        $results[] = $new_field;
      }
    }

    return new ArrayIterator($results);
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    // We alter the count function to have the iterator into consideration.
    return $this->initializeIterator()->count();
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row, $keep = TRUE) {
    parent::prepareRow($row);

    self::setCountryFieldProperties($row);

    self::setEmFieldProperties($row);

    self::setSimpleGmapFieldProperties($row);

    self::convertFileToVideoEmbedField($row);

    self::setDateAttributes($row);

    // Fix issue with multiple instances of a text_long
    // when using different text_processing per instance.
    Helper::enableTextProcessingOnMultipleInstances($row);

    return DrupalSqlBase::prepareRow($row);
  }

  /**
   * Convert file to video embed field dependent on specific conditions.
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   */
  protected static function convertFileToVideoEmbedField(Row $row) {
    // Only need to change values for files.
    if ($row->getSourceProperty('type') !== 'file') {
      return;
    }

    $instances = $row->getSourceProperty('instances');

    $allowed_types = [];
    $allowed_schemes = [];

    foreach ($instances as $instance) {
      $data = unserialize($instance['data']);

      if (isset($data['widget']['settings']['allowed_types'])) {
        foreach ($data['widget']['settings']['allowed_types'] as $key => $type) {
          if ($key === $type) {
            $allowed_types[] = $type;
          }
        }
      }

      if (isset($data['widget']['settings']['allowed_schemes'])) {
        foreach ($data['widget']['settings']['allowed_schemes'] as $key => $scheme) {
          if ($key === $scheme) {
            $allowed_schemes[] = $scheme;
          }
        }
      }
    }

    // The most used schemas are vimeo and youtube so we check only for those,
    // but others can be added.
    $has_allowed_schemes = !empty(array_intersect($allowed_schemes, [
      'vimeo',
      'youtube',
    ]));
    $has_only_video = count($allowed_types) === 1 && $allowed_types[0] === 'video';

    if ($has_only_video || $has_allowed_schemes) {
      $row->setSourceProperty('type', 'video_embed_field');
    }
    //TODO: handle D7 media fields with video upload and youtube videos.
  }

  /**
   * Set the simple_gmap properties so it gets migrated to an address.
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   */
  protected static function setSimpleGmapFieldProperties(Row $row) {
    if (Helper::isMigrationFieldRowSimpleGmap($row->getSourceProperty('field_name'))) {
      // Set the properties so the field will be migrated as an address.
      $row->setSourceProperty('type', 'addressfield');
      $row->setSourceProperty('module', 'address');
    }
  }

  /**
   * Set the emfield properties so it gets migrated to an video_embed_field.
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   */
  protected static function setEmFieldProperties(Row $row) {
    // Only need to change values for files.
    if ($row->get('type') !== 'file') {
      return;
    }

    // Check the instance data to decide if the migration change should happen.
    $first_instance_data = $row->get('instances')[0]['data'] ?? [];

    // Nothing to change without data.
    if (empty($first_instance_data)) {
      return;
    }

    $first_instance_data = unserialize($first_instance_data);

    // Set the properties so the field will be migrated as an video_embed_field.
    if ($first_instance_data['widget']['type'] === 'enfield_widget') {
      $row->setSourceProperty('type', 'video_embed_field');
      $row->setSourceProperty('module', 'emfield');
    }
  }

  /**
   * Set the country field properties so it gets migrated to an list_string
   * field.
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   */
  protected static function setCountryFieldProperties(Row $row) {
    // Only need to change values for files.
    if ($row->get('type') !== 'country') {
      return;
    }

    // Create settings array.
    $settings = [
      'allowed_values' => Helper::getCountries(),
    ];

    $row->setSourceProperty('settings', $settings);
  }

  /**
   * Set the proper settings for date fields.
   *
   * @param \Drupal\migrate\Row $row
   *   The migration row.
   */
  protected static function setDateAttributes(Row $row) {
    // If the field is a datetime, we will check if the option to set a
    // "to date" is set and we'll change its type to daterange in order to be
    // able to set this value.
    if ($row->get('type') === 'datetime' || $row->get('type') === 'datestamp') {
      $settings = $row->get('settings');
      if (isset($settings['todate']) && !empty($settings['todate'])) {
        $row->setSourceProperty('type', 'daterange');
        $row->setSourceProperty('module', 'datetime_range');

        if ($settings['todate'] == 'optional') {
          $settings['optional_end_date'] = 1;
          $row->setSourceProperty('settings', $settings);
        }
      }
      if (isset($settings['granularity'])) {
        // If all off these are 0 we can assume we only have to store date.
        $hour = $settings['granularity']['hour'] ?? 0;
        $minute = $settings['granularity']['minute'] ?? 0;
        $second = $settings['granularity']['minute'] ?? 0;

        if ($hour === 0 && $minute === 0 && $second === 0) {
          $settings['datetime_type'] = 'date';
          $row->setSourceProperty('settings', $settings);
        }
      }
    }
  }

}
