<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\field\Plugin\migrate\source\d7\FieldInstancePerViewMode;
use Drupal\migrate\MigrateLookupInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\openfed_migrate\Helper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The field instance per view mode source class.
 *
 * @MigrateSource(
 *   id = "d7_field_instance_per_view_mode",
 *   source_module = "field"
 * )
 */
class OpenfedFieldInstancePerViewMode extends FieldInstancePerViewMode {

  /**
   * The migrate lookup.
   *
   * @var \Drupal\migrate\MigrateLookupInterface
   */
  protected $migrateLookup;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, MigrateLookupInterface $migrate_lookup) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
    $this->migrateLookup = $migrate_lookup;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('migrate.lookup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // This fixes some issues that occur when comment module is disabled but
    // fields exist in Openfed 7 table (possibly due to features).
    if (!$this->moduleExists('comment')) {
      $query->condition('fci.entity_type', 'comment', 'NOT IN');
    }

    // This fixes some issues that occur when bean module is disabled but
    // fields exist in Openfed 7 table.
    if (!$this->moduleExists('bean')) {
      $query->condition('fci.entity_type', 'bean', 'NOT IN');
    }

    // Check for entity types.
    $schema = $this->getDatabase()->schema();
    Helper::sourceQueryAlter($query, $schema);

    // Exclude D7 default file_entity fields since they were renamed in D8.
    Helper::ignoreFields($query);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $it = parent::initializeIterator();
    $rows = [];

    // Loop through each element in the iterator.
    while($it->valid()) {
      $row = $it->current();
      // Adds the current row to the final array
      // if the view mode has been migrated
      // during upgrade_d7_view_modes migration.
      if (!empty($this->migrateLookup->lookup('upgrade_d7_view_modes', ['entity_type' => $row['entity_type'], 'view_mode' => $row['view_mode']]))) {
        $rows[$it->key()] = $row;
      }
      $it->next();
    }

    // TODO: see if we really need to add the new _ext field to the display.
//    foreach ($it as $index => $result) {
//      if ($result['type'] == 'location') {
//        // Every location field type will be split in two. This is the alternative
//        // in D8 to store coordinates (geolocation) and address (address).
//        $rows[$index]['type'] = 'geolocation';
//        foreach ($result['instances'] as &$field_instance) {
//          $field_instance['type'] = 'geolocation';
//        }
//
//        $new_field = $result;
//        $new_field['id'] .= 'new';
//        $new_field['field_id'] .= 'new';
//        $new_field['field_name'] .= '_ext';
//        $new_field['type'] = 'address';
//        foreach ($new_field['instances'] as &$instance) {
//          $instance['type'] = 'address';
//        }
//        $rows[] = $new_field;
//      }
//    }

    return new \ArrayIterator($rows);
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function prepareRow(Row $row) {

    //    // Change bean entity_type to bean.
    //    if ($row->getSourceProperty('entity_type') == 'bean') {
    //      $row->setSourceProperty('entity_type', 'block_content');
    //    }

    // We need to change the formatter. Some modules, like display suite and
    // file entity, don't provide the same formatters anymore so we need to
    // replace them.
    $formatter = $row->getSourceProperty('formatter');
    $formatter_type = $formatter['type'];

    // This is a keyed array, by D7 formatter, containing the new formatter and
    // its settings.
    $formatter_mapping = [
      'ds_taxonomy_view_mode' => [
        'plugin' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => $formatter['settings']['taxonomy_term_reference_view_mode']
        ],
      ],
      'file_rendered' => [
        'plugin' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => $formatter['settings']['file_view_mode']
        ],
      ],
      'ds_taxonomy_separator_localized' => [
        'plugin' => 'entity_reference_entity_view',
        'settings' => [
          'separator' => $formatter['settings']['taxonomy_term_separator'],
          'linked' => $formatter['settings']['taxonomy_term_link'],

        ],
      ],
      'taxonomy_term_reference_plain' => [
        'plugin' => 'cshs_full_hierarchy',
        'settings' => [
          'separator' => $formatter['settings']['taxonomy_term_separator'],
          'linked' => $formatter['settings']['taxonomy_term_link'],

        ],
      ],
      'i18n_taxonomy_term_reference_plain' => [
        'plugin' => 'entity_reference_label',
        'settings' => [
          'link' => TRUE,
        ],
      ],
      'smart_trim_format' => [
        'plugin' => 'smart_trim',
        'settings' => $formatter['settings'],
      ],
      'i18n_list_default' => [
        'plugin' => 'list_default',
        'settings' => $formatter['settings'],
      ],
      'text_plain' => [
        'plugin' => 'basic_string',
        'settings' => $formatter['settings'],
      ],
      'date_default' => [
        'plugin' => 'datetime_default',
        'settings' => $formatter['settings'],
      ],
      'link_url' => [
        'plugin' => 'link',
        'settings' => [
          'trim_length' => '',
          'url_only' => TRUE,
        ],
      ],
      'geofield_leaflet' => [
        'plugin' => 'leaflet_formatter_default',
        // Settings should be set manually.
        'settings' => [],
      ],
      'hs_taxonomy_term_reference_hierarchical_text' => [
        'plugin' => 'cshs_flexible_hierarchy',
        'settings' => [],
      ],
      'field_collection_fields' => [
        'plugin' => 'entity_reference_revisions_entity_view',
        'settings' => [
          'view_mode' => 'default',
        ],
      ],
      'location_default' => [
        'plugin' => 'address_default',
        'settings' => [],
      ],
      'location_all' => [
        'plugin' => 'address_default',
        'settings' => [],
      ],
      'location_multiple' => [
        'plugin' => 'address_default',
        'settings' => [],
      ],
      'location_map' => [
        'plugin' => 'geolocation_address',
        'settings' => [],
      ],
    ];

    if ($formatter_type == 'date_default') {
      $data = unserialize($row->getSourceProperty('data'));

      if (isset($data['settings']['default_value2'])) {
        $formatter_mapping[$formatter_type]['plugin'] = 'daterange_default';
      }
    }

    if (isset($formatter_mapping[$formatter_type])) {
      $row->setSourceProperty('formatter/type', $formatter_mapping[$formatter_type]['plugin']);
      $row->setSourceProperty('formatter/settings', $formatter_mapping[$formatter_type]['settings']);
    }

    // Fields with type text which used to show a simple_gmap are now address
    // fields and the display should be adjusted.
    if (Helper::isMigrationFieldRowSimpleGmap($row->getSourceProperty('field_name'))) {
      if ($formatter_type == 'simple_gmap') {
        $row->setSourceProperty('formatter/type', 'geolocation_address');
        $settings = [
          'data_provider_settings' => ['geocoder' => 'photon'],
          'map_provider_id' => 'leaflet',
          'map_provider_settings' => [
            'height' => $formatter['settings']['iframe_height'],
            'width' => $formatter['settings']['iframe_width'],
            'zoom' => $formatter['settings']['zoom_level'],
          ]
        ];
        $row->setSourceProperty('formatter/settings', $settings);
      }
    }

    // Fix issue with multiple instances of a text_long
    // when using different text_processing per instance.
    Helper::enableTextProcessingOnMultipleInstances($row);

    return parent::prepareRow($row);
  }

}
