<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\pathauto;

use Drupal\Component\Uuid\Php;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fetches pathauto patterns from the source database.
 * Same as PathautoPatern Class, but a different prepareRow().
 *
 * @MigrateSource(
 *   id = "pathauto_pattern",
 *   source_module = "pathauto"
 * )
 */
class OpenfedPathautoPattern extends DrupalSqlBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The uuid generator interface.
   *
   * @var \Drupal\Component\Uuid\Php
   */
  protected $uuid;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityManagerInterface $entity_manager, EntityTypeManagerInterface $entity_type_manager, Php $uuid) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_manager);
    $this->entityTypeManager = $entity_type_manager;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity.manager'),
      $container->get('entity_type.manager'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Fetch all pattern variables whose value is not a serialized empty string.
    return $this->select('variable', 'v')
      ->fields('v', ['name', 'value'])
      ->condition('name', 'pathauto_%_pattern', 'LIKE')
      ->condition('value', 's:0:"";', '<>');
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['name']['type'] = 'string';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'name' => $this->t("The name of the pattern's variable."),
      'value' => $this->t("The value of the pattern's variable."),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $entity_definitions = $this->entityTypeManager->getDefinitions();
    $name = $row->getSourceProperty('name');
    $weight = '-52';
    // Pattern variables are made of pathauto_[entity type]_[bundle]_pattern.
    // First let's find a matching entity type from the variable name.
    foreach ($entity_definitions as $entity_type => $definition) {
      // Check if this is the default pattern for this entity type.
      // Otherwise, check if this is a pattern for a specific bundle.
      if ($name == 'pathauto_' . $entity_type . '_pattern') {
        // We want default bundles to have less priority than specific patterns
        // for content type or language. A pattern bundle is default if the
        // label is "default".
        $weight = '-51';

        // Process pattern in order to include field_menulink, if the module is
        // enabled, instead of Core menu link.
        $pattern = $this->processPattern(unserialize($row->getSourceProperty('value')));

        // Set process values.
        $row->setSourceProperty('id', $entity_type);
        $row->setSourceProperty('label', (string) $definition->getLabel() . ' - default');
        $row->setSourceProperty('type', 'canonical_entities:' . $entity_type);
        $row->setSourceProperty('pattern', $pattern);
        $row->setSourceProperty('weight', $weight);
        return parent::prepareRow($row);
      }
      elseif (strpos($name, 'pathauto_' . $entity_type . '_') === 0) {
        // Extract the bundle out of the variable name.
        preg_match('/^pathauto_' . $entity_type . '_([a-zA-z0-9_-]+)_pattern$/', $name, $matches);
        $bundle = $matches[1];

        // Getting all bundles.
        $bundles = $this->entityManager->getBundleInfo($entity_type);

        // Maybe because the bundle has langcode attached to it. Let's check
        // that and try to split it.
        $pattern_langcode = '';
        $active_langcodes = array_keys(\Drupal::languageManager()
          ->getLanguages());
        foreach ($active_langcodes as $langcode) {
          preg_match('/^([a-z_]*)_' . $langcode . '$/', $bundle, $striped_bundle);
          if (isset($striped_bundle[1])) {
            // This means that there is a langcode attached to the bundle type
            // so we'll check if it exists.
            if (in_array($striped_bundle[1], array_keys($bundles))) {
              $bundle = $striped_bundle[1];
              $pattern_langcode = $langcode;
              break;
            }
          }
        }

        // Check that the bundle exists.
        if (!in_array($bundle, array_keys($bundles))) {
          // No matching bundle found in destination.
          return FALSE;
        }

        // Process pattern in order to include field_menulink, if the module is
        // enabled, instead of Core menu link.
        $pattern = $this->processPattern(unserialize($row->getSourceProperty('value')));
        // Process the field name in the pattern in order to replace "-" with "_".
        $pattern = $this->processFieldPattern($entity_type, $bundle, $pattern);

        // Set process values.
        $row->setSourceProperty('id', $entity_type . '_' . $bundle);
        $row->setSourceProperty('label', (string) $definition->getLabel() . ' - ' . $bundles[$bundle]['label']);
        $row->setSourceProperty('type', 'canonical_entities:' . $entity_type);
        $row->setSourceProperty('pattern', $pattern);
        $row->setSourceProperty('weight', 0);
        $row->setSourceProperty('relationships', []);

        $selection_criteria[] = [
          'id' => ($entity_type == 'node') ? 'node_type' : 'entity_bundle:' . $entity_type,
          'bundles' => [$bundle => $bundle],
          'negate' => FALSE,
          'context_mapping' => [$entity_type => $entity_type],
        ];

        // Set the language selection criteria, if language set.
        if ($pattern_langcode) {
          // We start by adding an uuid to node selection criteria.
          $uuid = $this->uuid->generate();
          $selection_criteria[$uuid] = $selection_criteria[0];
          unset($selection_criteria[0]);
          $selection_criteria[$uuid]['uuid'] = $uuid;

            // Generate a new uuid.
          $uuid = $this->uuid->generate();
          $selection_criteria[$uuid] = [
            'id' => 'language',
            'langcodes' => [$pattern_langcode => $pattern_langcode],
            'negate' => FALSE,
            'context_mapping' => ['language' => $entity_type . ':langcode:language'],
            'uuid' => $uuid
          ];

          // We will give priority to a path pattern that has a language because
          // it will be more specific. As such, we'll change the weight.
          $weight = '-53';

          // We also need to update label and id to distinguish from default
          // patterns.
          $row->setSourceProperty('id', $entity_type . '_' . $bundle . '_' . $pattern_langcode);
          $row->setSourceProperty('label', (string) $definition->getLabel() . ' - ' . $bundles[$bundle]['label'] . ' ' . $pattern_langcode);

          // Set the necessary relationships.
          $row->setSourceProperty('relationships', [$entity_type . ':langcode:language' => ['label' => 'Language']]);
        }

        $row->setSourceProperty('weight', $weight);
        $row->setSourceProperty('selection_criteria', $selection_criteria);
        return parent::prepareRow($row);
      }
    }

    return FALSE;
  }

  /**
   * Processes the pattern in order to use menu_link field if the module exists.
   *
   * @param string
   *  The path pattern to process.
   *
   * @return string
   *  The processed path pattern.
   */
  private function processPattern($pattern) {
    // Process pattern in order to include field_menulink, if the module is
    // enabled, instead of Core menu link.
    if ($this->getModuleHandler()->moduleExists('menu_link')) {
      if (strpos($pattern, '[node:menu-link:parents:join-path]') !== FALSE || strpos($pattern, '[node:menu-link:parent:url:path]') !== FALSE) {
        // Menu Link field name is predefined on Openfed Migration so we can
        // safely use it.
        $pattern = str_replace('[node:menu-link:parents:join-path]', '[node:field_menulink:join-parents-path]', $pattern);
        $pattern = str_replace('[node:menu-link:parent:url:path]', '[node:field_menulink:join-parents-path]', $pattern);
      }
    }

    // Remove "i18n-" from the pattern.
    if (strpos($pattern, 'i18n-') !== FALSE) {
      $pattern = str_replace('i18n-', '', $pattern);
    }

    return $pattern;
  }

  /**
   * Processes the field name in the pattern in order to replace "-" with "_".
   * It also includes special replacements for entity reference fields.
   *
   * @param $entity_type
   *   The entity type where the pattern is being used.
   * @param $bundle
   *   The corresponding bundle.
   * @param $pattern
   *   The pathauto pattern.
   *
   * @return string
   */
  protected function processFieldPattern($entity_type, $bundle, $pattern) {
    preg_match_all('(field-.+?(?=:))', $pattern, $match_field_patterns);

    if (!empty($match_field_patterns[0])){
      foreach (array_unique($match_field_patterns[0]) as $field_pattern) {
        $new_field_pattern = str_replace('-', '_', $field_pattern);
        $pattern = str_replace($field_pattern, $new_field_pattern, $pattern);

        // Do special replacements on entity reference fields
        // since in D7 the token doesn't include the "entity" key
        // to access the entity values.
        $entity_ref_token_mappings = [
          'd7' => "[$entity_type:$new_field_pattern:0:",
          'd8' => "[$entity_type:$new_field_pattern:0:entity:"
        ];
        if (strpos($pattern, $entity_ref_token_mappings['d7']) !== FALSE) {
          if ($field_definition = FieldConfig::loadByName($entity_type, $bundle, $new_field_pattern)) {
            if ($field_definition->get('field_type') === 'entity_reference') {
              $pattern = str_replace($entity_ref_token_mappings['d7'], $entity_ref_token_mappings['d8'], $pattern);
            }
          }
        }
      }
    }

    return $pattern;
  }

}
