<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\taxonomy;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;
use Drupal\taxonomy\Plugin\migrate\source\d7\Term;

/**
 * Taxonomy term source from database.
 *
 * @MigrateSource(
 *   id = "d7_taxonomy_term_final_fixes",
 *   source_module = "taxonomy"
 * )
 */
class OpenfedTermFinalFixes extends Term {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // We should avoid to migrate terms if they are managed by menu_to_taxonomy.
    // We check if menu_to_taxonomy is being used in D8 and we assume it is used
    // also in D7 and we add a query to skip rows.
    // Taxonomy fields using menu_to_taxonomy in D7 should be using a
    // "Taxonomy Term (Menu to Taxonomy Assigned)" field type. This field is
    // provided by menu_to_taxonomy_assign and must be create manually.
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('menu_to_taxonomy')) {
      $subquery = $this->select('menu_to_taxonomy', 'mtt')->distinct();
      $subquery->addField('mtt', 'vid');
      $query->condition('td.vid', $subquery, 'NOT IN');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    parent::prepareRow($row);
    $parent = $row->getSourceProperty('parent');
    $row->setSourceProperty('parent', $parent[0]);
    return FieldableEntity::prepareRow($row);
  }
}
