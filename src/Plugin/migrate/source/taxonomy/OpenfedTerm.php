<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\taxonomy;

use Drupal\migrate\Row;
use Drupal\taxonomy\Plugin\migrate\source\d7\Term;

/**
 * Taxonomy term source from database.
 *
 * @MigrateSource(
 *   id = "d7_taxonomy_term",
 *   source_module = "taxonomy"
 * )
 */
class OpenfedTerm extends Term {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // We need a subquery to retrieve the list of term ids, grouped by i18n_tsid
    // and threat them as the source term for translations.
    $subquery = $this->select('taxonomy_term_data', 'td');
    $subquery->addExpression('MIN(td.tid)');
    $subquery->condition('tv.i18n_mode', 4);
    $subquery->groupBy('td.i18n_tsid');
    $subquery->leftJoin('taxonomy_vocabulary', 'tv', 'td.vid = tv.vid');

    if (isset($this->configuration['bundle'])) {
      $subquery->condition('tv.machine_name', (array) $this->configuration['bundle'], 'IN');
    }

    // we do a copy of the subquery to limit the number of results in order to
    // avoid errors on the isNull check below.
    $subquery_copy = clone $subquery;
    $subquery_copy->range(0, 1);

    // Or condition to add the query if there are values or check it if empty.
    $orGroup = $query->orConditionGroup()
      ->condition('td.tid', $subquery, 'IN')
      ->condition('td.i18n_tsid', 0)
      ->isNull($subquery_copy);
    // Add the group to the query.
    $query->condition($orGroup);

    // We need to sort the results so we get always the same output.
    $query->orderBy('td.tid', 'ASC');

    // We should avoid to migrate terms if they are managed by menu_to_taxonomy.
    // We check if menu_to_taxonomy is being used in D8 and we assume it is used
    // also in D7 and we add a query to skip rows.
    // Taxonomy fields using menu_to_taxonomy in D7 should be using a
    // "Taxonomy Term (Menu to Taxonomy Assigned)" field type. This field is
    // provided by menu_to_taxonomy_assign and must be create manually.
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('menu_to_taxonomy')) {
      $subquery = $this->select('menu_to_taxonomy', 'mtt')->distinct();
      $subquery->addField('mtt', 'vid');
      $query->condition('td.vid', $subquery, 'NOT IN');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $preparedRow = parent::prepareRow($row);

    return $preparedRow;
  }

}
