<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\taxonomy;

use Drupal\content_translation\Plugin\migrate\source\I18nQueryTrait;
use Drupal\migrate\Row;
use Drupal\taxonomy\Plugin\migrate\source\d7\TermTranslation;

/**
 * Gets i18n taxonomy terms from source database.
 *
 * @MigrateSource(
 *   id = "d7_taxonomy_term_translation",
 *   source_module = "i18n_taxonomy"
 * )
 */
class OpenfedTermTranslation extends TermTranslation {

  use I18nQueryTrait;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // We only need to translate terms with a i18n_mode "Translatable"
    if ($this->database->schema()->fieldExists('taxonomy_vocabulary', 'i18n_mode')) {
      $query->condition('i18n_mode', 4);
    }

    // We should merge the current query with a list of i18n_tsids items, in
    // order to get the original term for the translation.
    $subquery = $this->select('taxonomy_term_data', 'td');
    $subquery->fields('td', ['i18n_tsid']);
    $subquery->addExpression('MIN(td.tid)', 'ttid');
    $subquery->condition('tv.i18n_mode', 4);
    $subquery->condition('i18n_tsid', 0, '>'); // Only tsid above zero are translations.
    $subquery->groupBy('td.i18n_tsid');
    $subquery->leftJoin('taxonomy_vocabulary', 'tv', 'td.vid = tv.vid');

    // We will join with the subquery to exclude already migrated items on
    // d7_taxonomy_term.
    $query->innerJoin($subquery, 'sq', 'sq.i18n_tsid=td.i18n_tsid AND td.tid<>sq.ttid');
    $query->fields('sq', ['ttid']);

    // We should avoid to migrate terms if they are managed by menu_to_taxonomy.
    // We check if menu_to_taxonomy is being used in D8 and we assume it is used
    // also in D7 and we add a query to skip rows.
    // Taxonomy fields using menu_to_taxonomy in D7 should be using a
    // "Taxonomy Term (Menu to Taxonomy Assigned)" field type. This field is
    // provided by menu_to_taxonomy_assign and must be create manually.
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('menu_to_taxonomy')) {
      $subquery = $this->select('menu_to_taxonomy', 'mtt')->distinct();
      $subquery->addField('mtt', 'vid');
      $query->condition('td.vid', $subquery, 'NOT IN');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
//    $test = '';
//
//    if ($row->getSourceProperty('i18n_tsid') != 0) {
//      $database = $this->getDatabase()->schema();
//      $query = $database->select('taxonomy_term_data', 'td')->fields('td');
//      $query->condition('td.i18n_tsid', $row->getSourceProperty('i18n_tsid'));
//      $i18_group_translations = $query->execute()->fetchAll();
//    }

    // TODO: set the correct parent, according to the translation.
//    // Find parents for this row.
//    $parents = $this->select('taxonomy_term_hierarchy', 'th')
//      ->fields('th', ['parent', 'tid'])
//      ->condition('tid', $row->getSourceProperty('tid'))
//      ->execute()
//      ->fetchCol();
//    $row->setSourceProperty('parent', $parents);

    parent::prepareRow($row);
  }

}
