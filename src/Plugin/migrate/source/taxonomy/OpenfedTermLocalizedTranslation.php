<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\taxonomy;

use Drupal\taxonomy\Plugin\migrate\source\d7\TermLocalizedTranslation;

/**
 * Gets i18n taxonomy terms from source database.
 *
 * @MigrateSource(
 *   id = "openfed_d7_term_localized_translation",
 *   source_module = "i18n_taxonomy"
 * )
 */
class OpenfedTermLocalizedTranslation extends TermLocalizedTranslation {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    // We only want to migrate terms of a vocabulary
    // with i18n_mode = 1.
    $query->condition('tv.i18n_mode', '1');

    // We should avoid to migrate terms if they are managed by menu_to_taxonomy.
    // We check if menu_to_taxonomy is being used in D8 and we assume it is used
    // also in D7 and we add a query to skip rows.
    // Taxonomy fields using menu_to_taxonomy in D7 should be using a
    // "Taxonomy Term (Menu to Taxonomy Assigned)" field type. This field is
    // provided by menu_to_taxonomy_assign and must be create manually.
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('menu_to_taxonomy')) {
      $subquery = $this->select('menu_to_taxonomy', 'mtt')->distinct();
      $subquery->addField('mtt', 'vid');
      $query->condition('td.vid', $subquery, 'NOT IN');
    }

    return $query;
  }

}
