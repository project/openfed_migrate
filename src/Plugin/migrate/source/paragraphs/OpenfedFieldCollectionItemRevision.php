<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\paragraphs;

use Drupal\paragraphs\Plugin\migrate\source\d7\FieldCollectionItemRevision;

/**
 * Field Collection Item Revision source plugin.
 *
 * Available configuration keys:
 * - field_name: (optional) If supplied, this will only return field collections
 *   of that particular type.
 *
 * @MigrateSource(
 *   id = "d7_field_collection_item_revision",
 *   source_module = "field_collection",
 * )
 */
class OpenfedFieldCollectionItemRevision extends FieldCollectionItemRevision {

  /**
   * Join string for getting all except the current revisions.
   */
  const JOIN = "f.item_id = fr.item_id";

}
