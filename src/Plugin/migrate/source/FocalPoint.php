<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source;

use ArrayIterator;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Image\Image;
use Drupal\Core\ImageToolkit\ImageToolkitManager;
use Drupal\Core\State\StateInterface;
use Drupal\focal_point\FocalPointManagerInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Source plugin for 'Focal point'.
 *
 * @MigrateSource(
 *   id = "d7_focal_point",
 *   source_module = "focal_point"
 * )
 */
class FocalPoint extends DrupalSqlBase {

  /**
   * The image toolkit manager.
   *
   * @var \Drupal\Core\ImageToolkit\ImageToolkitManager
   */
  protected $imageToolkitManager;

  /**
   * The focal point manager.
   *
   * @var \Drupal\focal_point\FocalPointManagerInterface
   */
  protected $focalPointManager;

  /**
   * FocalPoint constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration interface.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state manager interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   * @param \Drupal\Core\ImageToolkit\ImageToolkitManager $image_toolkit_manager
   *   The image toolkit manager interface.
   * @param \Drupal\focal_point\FocalPointManagerInterface $focal_point_manager
   *   The focal point manger interface.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, ImageToolkitManager $image_toolkit_manager, $focal_point_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
    $this->imageToolkitManager = $image_toolkit_manager;
    $this->focalPointManager = $focal_point_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    if (\Drupal::hasService('focal_point.manager')) {
      $focal_point_manager = $container->get('focal_point.manager');
    } else {
      $focal_point_manager = null;
    }
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('image.toolkit.manager'),
      $focal_point_manager
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('focal_point', 'fp')->fields('fp');
    // Join the files so we can pass the uri to the crop_field_data.
    $query->join('file_managed', 'fm', 'fm.fid = fp.fid');
    $query->fields('fm', ['uri']);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'fid' => $this->t('The file id'),
      'focal_point' => $this->t('The focal point value.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'fid' => [
        'type' => 'integer',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $results = $this->prepareQuery()->execute()->fetchAll();

    $focal_points = [];

    foreach ($results as $result) {
      // This needs to be called in a loop to avoid this error:
      // ImageToolkitBase::setSource() may only be called once.
      $toolkit = $this->imageToolkitManager->getDefaultToolkit();
      // Create an image object to retrieve the width and height.
      $image = new Image($toolkit, $result['uri']);
      // Focal points in d7 are stored like: x,y.
      $coords = explode(',', $result['focal_point']);

      // Calculate the focal points.
      $focal_point = $this->focalPointManager->relativeToAbsolute($coords[0], $coords[1], $image->getWidth(), $image->getHeight());

      $focal_points[] = [
        'fid' => $result['fid'],
        // The default crop type created by the focal_point module.
        'type' => 'focal_point',
        'uri' => $result['uri'],
        'x' => $focal_point['x'],
        'y' => $focal_point['y'],
      ];
    }

    return new ArrayIterator($focal_points);
  }

}
