<?php

namespace Drupal\openfed_migrate\Plugin\migrate\source\block;

use Drupal\block\Plugin\migrate\source\Block;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\MigrateLookupInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal block source from database.
 *
 * @MigrateSource(
 *   id = "block",
 *   source_module = "block"
 * )
 */
class OpenfedBlock extends Block {

  /**
   * Table listing i18n block language visibility settings.
   *
   * @var string
   */
  protected $blockLanguageTable;

  /**
   * Table listing block content type visibility settings.
   *
   * @var string
   */
  protected $blockContentTypeTable;

  /**
   * The migration plugin manager service.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * The migrate lookup service.
   *
   * @var \Drupal\migrate\MigrateLookupInterface
   */
  protected $migrateLookup;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, MigrationPluginManagerInterface $migration_plugin_manager, MigrateLookupInterface $migrate_lookup) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->migrateLookup = $migrate_lookup;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.migration'),
      $container->get('migrate.lookup')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function query() {
    // We'll alter the original query to get only the default theme blocks since
    // we don't really care about other theme blocks (not even admin theme).
    $query = parent::query();

    $default_theme = $this->variableGet('theme_default', 'Garland');

    // We add an extra query to include the language and content type visibility
    // options, for all blocks.
    $this->blockLanguageTable = 'i18n_block_language';
    $this->blockContentTypeTable = 'block_node_type';

    // We get only blocks assigned to the frontend theme.
    $query->condition('theme', $default_theme, 'LIKE');
    // We get only blocks with a region assigned, which means, different from
    // -1. This is the same has having the status equals to zero.
    $query->condition('region', '-1', '<>');
    // We can also ignore views and facet blocks since they will have to be
    // created manually.
    $query->condition('module', [
      'views',
      'facetapi',
      'search',
      'sharethis',
    ], 'NOT IN');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $module = $row->getSourceProperty('module');
    $delta = $row->getSourceProperty('delta');

    // Query to get block language visibility options.
    $query = $this->select($this->blockLanguageTable, 'bl')
      ->fields('bl', ['language'])
      ->condition('module', $module)
      ->condition('delta', $delta);
    $languages = $query->execute()->fetchAllKeyed(0, 0);
    $row->setSourceProperty('language', $languages);

    // Query to get block content type visibility options.
    $query = $this->select($this->blockContentTypeTable, 'bct')
      ->fields('bct', ['type'])
      ->condition('module', $module)
      ->condition('delta', $delta);
    $content_type = $query->execute()->fetchAllKeyed(0, 0);
    $row->setSourceProperty('content_type', $content_type);

    // We'll get the block title and check if it is a valid value. If not, we'll
    // get a valid title so we can pass it to OpenfedBlockSettings and change
    // it.
    $title = $row->getSourceProperty('title');

    $settings = [];
    switch ($module) {
      case 'block':
        if ($title == '<none>') {
          $title = $this->select('block_custom', 'bc')
            ->fields('bc', ['info'])
            ->condition('bid', $delta)
            ->execute()
            ->fetchField();
          $row->setSourceProperty('alternative_title', $title);
        }
        break;
      case 'bean':
        if ($title == '<none>') {
          $title = $this->select('bean', 'b')
            ->fields('b', ['label'])
            ->condition('delta', $delta)
            ->execute()
            ->fetchField();
          $row->setSourceProperty('alternative_title', $title);
        }
        break;
      case 'menu':
        if ($title == '<none>') {
          $title = $this->select('menu_custom', 'mc')
            ->fields('mc', ['title'])
            ->condition('menu_name', $delta)
            ->execute()
            ->fetchField();
          $row->setSourceProperty('alternative_title', $title);
        }
        break;
      case 'menu_block':
        // We need a default menu since some menu_blocks in D7 don't define a
        // menu but use the active trail to look for the active menu and render
        // the menu tree. We will set a default menu for these cases, which will
        // require manual intervention once the migration is finalized.
        $default_menu = 'main';
        // Mapping between D7 menu_block variable names and D8 menu_block
        // settings. The D8 settings include the setting id and a default value.
        $mapping = [
          'admin_title' => ['label', ''],
          'class_name' => ['suggestion', 'menu_block'],
          'depth' => ['depth', 0],
          'expanded' => ['expand', 0],
          'follow' => ['relative', 0],
          'level' => ['level', 1],
          'relative' => ['follow', ''],
        ];

        // Getting the D7 settings and mapping them on a D8 structure.
        foreach ($mapping as $d7key => $d8values) {
          if ($d7key) {
            list($d8key, $d8default) = $d8values;
            $d7value = $this->variableGet('menu_block_' . $delta . '_' . $d7key, '');
            $settings[$d8key] = $d7value ?: $d8default;
          }
        }

        // Update the default menu, if we can get it from D7 menu_block configs.
        $parent = $this->variableGet('menu_block_' . $delta . '_parent', '');
        if (strpos($parent, '_active') !== 0) {
          $parent_menu = str_replace(':0', '', $parent);
          $parent_menu_name = reset(preg_split('/:/', $parent));

          // In Drupal8 the main-menu is mapped to main, which is already the
          // default_menu value. We only change the default value if the
          // parent_menu_name is different from main-menu otherwise we will be using
          // the wrong menu id.
          if ($parent_menu_name != 'main-menu') {
            $default_menu = $parent_menu_name;
          }
          else {
            $parent_menu = str_replace('main-menu', $default_menu, $parent_menu);
          }

          // Additional settings needed by the D8 menu_block plugin.
          $settings['id'] = $module . ':' . $default_menu . ':' . $delta;
          $settings['parent'] = $default_menu . ':';

          // If we have a fixed parent menu link
          // then we need to remove the mlid from
          // the menu name for the block_plugin_id
          // process to work.
          $parent_menu_values = explode(':', $parent_menu);
          if (count($parent_menu_values) > 1) {

            // We set the fixed menu link content
            // in the parent settings with
            // the correct D8 pattern.
            if ($menu_link_content = $this->entityTypeManager->getStorage('menu_link_content')->load($parent_menu_values[1])) {
              $settings['parent'] = "$default_menu:menu_link_content:{$menu_link_content->uuid()}";
            }
            else {
              if ($parent_menu_link = $this->getMenuBlockMenuLinkParent($parent_menu_values[1], $default_menu)) {
                $settings['parent'] = $parent_menu_link;
              }
            }
          }

        } else {
          // This is a menu block using the active trail and as such it uses the
          // Multiple Menu Block plugin.
          $default_menu = 'multiple_menu_block';

          // get the selected menus.
          $selected_menus = $this->variableGet('menu_block_menu_order', '');
          $menus = $this->entityTypeManager->getStorage('menu')->loadMultiple();
          foreach ($menus as $menu_id => $menu_value) {
            $menu_options[$menu_id] = array_key_exists($menu_id, $selected_menus) ? $menu_id : 0;
          }
          natcasesort($menu_options);
          $settings['selected_menus'] = array_reverse($menu_options);

          // provide a block template suggestion.
          $settings['suggestion'] = 'multiple_menu_block';
          $settings['provider'] = 'menu_block';

          // Additional settings needed by the D8 menu_block plugin.
          $settings['id'] = 'multiple_menu_block:multiple_menu_block_' . $delta;
          $settings['parent'] = null;
        }
        $row->setSourceProperty('default_menu', $default_menu);

        // Set context, if we are using menu_block_settings_override.
        $moduleHandler = \Drupal::service('module_handler');
        if ($moduleHandler->moduleExists('menu_block_settings_override')) {
          $config = \Drupal::config('menu_block_settings_override.settings');
          $blocks_enabled = $config->get('blocks_enabled');
          if (!empty($blocks_enabled[$module . '_' . $delta])) {
            // This block is set on menu_block_settings_override config so we
            // add a context to the config.
            $settings['context_mapping']['node'] = '@node.node_route_context:node';
          }
        }

        // The admin title goes to a setting that may be used later on
        // OpenfedBlockSettings to set the right title.
        if ($title == '<none>') {
          $title = $this->variableGet('menu_block_' . $delta . '_admin_title', '');
          $row->setSourceProperty('alternative_title', $title);
        }

        // We need to set the settings on a "fake" settings property since it
        // overrides it later.
        $row->setSourceProperty('settings_original', $settings);
        break;
    }

    // Set the status to false if the block visibility is 1 ("Only the listed
    // pages") and there are no pages set.
    if ($row->getSourceProperty('visibility') == 1 && $row->getSourceProperty('pages') == ''){
      $row->setSourceProperty('status', false);
    }

    // To avoid conflicts with Openfed default blocks, we'll add more weight to
    // each block to display them on the bottom of the list.
    $row->setSourceProperty('weight', $row->getSourceProperty('weight') + 100);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    // We need to alter the original fields to add extra helpful fields, that
    // can be used later for block processing.
    $fields = parent::fields();
    $fields['language'] = $this->t('Language visibility list.');
    $fields['content_type'] = $this->t('Content type visibility list.');
  }

  /**
   * Gets the fixed parent menu link for the menu block.
   *
   * @param $mlid
   *   Menu link id from the D7.
   * @param $menu_name
   *   Menu name.
   *
   * @return bool|string
   *   String containing the D8 menu link pattern.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\migrate\MigrateException
   */
  private function getMenuBlockMenuLinkParent($mlid, $menu_name) {
    // We get the node ID and language associated with the link
    // joining menu_links and node tables from D7, .
    $menu_link_d7_query = $this->select('menu_links', 'ml');
    $menu_link_d7_query->join('node', 'n', "ml.link_path LIKE CONCAT('node/', n.nid)");
    $menu_link_d7_query->fields('n', ['nid', 'language', 'type']);
    $menu_link_d7_query->condition('ml.mlid', $mlid);
    $results = $menu_link_d7_query->execute()->fetchAssoc();

    if (!empty($results)) {
      $migration_plugin_definitions = [
        'd7_node_' . $results['type'],
        'upgrade_d7_node_' . $results['type'],
        'd7_node_translation_' . $results['type'],
        'upgrade_d7_node_translation_' . $results['type'],
      ];
      // We do a lookup through the migration map
      // table in order to get the destination node ID
      // and if found, then we return the menu link pattern.
      foreach ($migration_plugin_definitions as $plugin_definition) {
        if ($this->migrationPluginManager->hasDefinition($plugin_definition)) {
          $node_ids = $this->migrateLookup->lookup($plugin_definition, ['nid' => $results['nid']]);
          if (!empty($node_ids)) {
            if ($node_menu_link = $this->getMenuLinkFromNode(reset($node_ids)['nid'], $results['language'])) {
              return "$menu_name:$node_menu_link";
            }
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * Gets the menu link pattern from node.
   *
   * @param $nid
   *   The Node ID.
   * @param $langcode
   *   The Node language.
   *
   * @return bool|string
   *   String containing the D8 menu link pattern.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getMenuLinkFromNode($nid, $langcode) {
    if ($node = $this->entityTypeManager->getStorage('node')->load($nid)) {
      if ($node->get('field_menulink')->isEmpty() === FALSE) {
        return "menu_link_field:node_field_menulink_{$node->uuid()}_{$langcode}";
      }
    }
    return FALSE;
  }

}
