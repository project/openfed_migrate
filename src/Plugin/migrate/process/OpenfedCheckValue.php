<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "openfed_check_value"
 * )
 */
class OpenfedCheckValue extends ProcessPluginBase {

  /**
   * Process to check if a value is empty, returning an alternative.
   *
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // If check, return. Else, return link path.
    if ($this->configuration['check']) {
      $check = $row->get($this->configuration['check']);
      return $check ?: $row->get($this->configuration['default']);
    }
    return $value ?: $row->get($this->configuration['default']);
  }

}
