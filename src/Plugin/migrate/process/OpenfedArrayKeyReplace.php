<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Changes the source array key based on a lookup map.
 *
 * Available configuration keys:
 * - source: The input value - either a scalar or an array.
 * - map: An array that defines the mapping between source values and
 *   destination values.
 * - regex: (optional) Whether the mapping should be threated as a regex or as
 *   plain string to replace. Defaults to FALSE.
 *   - TRUE: mapping keys will be treated as a find replace.
 *   - FALSE: mappping key will be treated as a static mapping.
 *
 * Examples:
 *
 * If one key of the source array 'foo' is 'from' then the value of the
 * destination key will be 'to'. Similarly 'this' becomes 'that'.
 *
 * @code
 * process:
 *   bar:
 *     plugin: static_map
 *     source: foo
 *     map:
 *       from: to
 *       this: that
 * @endcode
 *
 * If one key of the source array 'foo' is 'from' then the value of the
 * destination key will be 'prom'.
 * @code
 * process:
 *   bar:
 *     plugin: static_map
 *     source: foo
 *     regex: TRUE
 *     map:
 *       f: p
 * @endcode
 *
 *  * @MigrateProcessPlugin(
 *   id = "openfed_array_key_replace"
 * )
 */
class OpenfedArrayKeyReplace extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $new_value = $value;
    $mapping = $this->configuration['map'];

    if (is_array($value)) {
      if (!$value) {
        throw new MigrateException('Can not lookup without a value.');
      }
    }
    else {
      throw new MigrateException('Can not lookup without an array.');
    }

    foreach ($value as $key => $item) {
      if (empty($this->configuration['regex'])) {
        if (array_key_exists($key, $mapping)) {
          $new_value[$mapping[$key]] = $item;
          unset($new_value[$key]);
        }
      }
      else {
        foreach ($mapping as $regex => $replacement) {
          $new_key = preg_replace("/$regex/", $replacement, $key);
          $new_value[$new_key] = $item;
          unset($new_value[$key]);
        }
      }
    }

    return $new_value;
  }

}
