<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\migrate\process\Download;
use Drupal\migrate\Row;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "download"
 * )
 */
class OpenfedDownload extends Download {

  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // We will add a try catch to avoid throwing an error if the file doesn't
    // exist. This file will be ignored anyway so why worrying the developer
    // with errors :)
    $final_destination = NULL;

    try {
      $final_destination = parent::transform($value, $migrate_executable, $row, $destination_property);
    } catch (MigrateException $e) {
      return $final_destination;
    }

    return $final_destination;
  }

}
