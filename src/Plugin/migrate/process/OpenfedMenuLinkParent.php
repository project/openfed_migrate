<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\migrate\process\MenuLinkParent;
use Drupal\migrate\Row;

/**
 * This plugin figures out menu link parent plugin IDs.
 *
 * @MigrateProcessPlugin(
 *   id = "openfed_menu_link_parent"
 * )
 */
class OpenfedMenuLinkParent extends MenuLinkParent {

  /**
   * {@inheritdoc}
   *
   * Find the parent link GUID.
   *
   * @throws \Drupal\migrate\MigrateSkipRowException
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $link = $this->menuLinkStorage->load($value);
    if ($link) {
      /** @var \Drupal\Core\Menu\MenuLinkInterface $link */
      return $link->getPluginId();
    }

    // Root
    return '';
  }

}
