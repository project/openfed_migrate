<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\openfed_migrate\Helper;

/**
 * Get the url's from the file id's.
 *
 * @MigrateProcessPlugin(
 *   id = "openfed_file_id_to_url"
 * )
 */
class OpenfedFileIdToUrl extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $file_id = $value['fid'] ?? '';

    if (empty($file_id)) {
      throw new MigrateSkipProcessException($this->t('No file id found.'));
    }

    $file = Helper::fileInfoLoad($file_id);

    if (!$file) {
      throw new MigrateSkipProcessException($this->t('File :id not found.', [
        ':id' => $file_id,
      ]));
    }

    $uri_parts = explode('/', $file->uri);
    $scheme = StreamWrapperManager::getScheme($file->uri);

    switch ($scheme) {
      case 'youtube':
        return 'https://www.youtube.com/watch?v=' . end($uri_parts);
      case 'vimeo':
        return 'https://vimeo.com/' . end($uri_parts);
      case 'soundcloud':
        return $this->getSoundcloudPath($uri_parts);
      default:
        return '';
    }
  }

  /**
   * Construct a valid soundcloud URL, based on interpolateUrl() from D7.
   */
  private function getSoundcloudPath($uri_parts) {
    $base_url = 'http://soundcloud.com/';
    $parameters = array('u','a','g','s');
    $url = "";

    foreach ($uri_parts as $index => $value) {
      if (in_array($value, $parameters, true)) {
        $parameters[$value] = $uri_parts[$index+1];
      }
    }

    // User.
    if (isset($parameters['u'])) {
      $url = $base_url . HTML::escape($parameters['u']);
    }

    // Group.
    if (isset($parameters['g'])) {
      $url = $base_url . 'groups/' . HTML::escape($parameters['g']);
    }

    // Single song.
    if (isset($parameters['u']) && isset($parameters['a'])) {
      $url = $base_url . HTML::escape($parameters['u']) . '/' . HTML::escape($parameters['a']);
    }

    // Audio sets.
    if (isset($parameters['u']) && isset($parameters['s'])) {
      $url = $base_url . HTML::escape($parameters['u']) . '/sets/' . HTML::escape($parameters['s']);
    }

    return $url;

  }

}
