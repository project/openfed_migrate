<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\menu_link;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Migrate data from Menu Link.
 *
 * @MigrateProcessPlugin(
 *   id = "menulink_entities",
 *   handle_multiples = TRUE
 * )
 */
class MenuLinkEntities extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    //    $test = '';
    //    // If there's no data, there's no need to store anything.
    //    if (empty($value)) {
    //      return NULL;
    //    }
    //
    ////    title
    ////    description
    ////    menu_name
    ////    parent
    ////    weight
    //
    //    $return = [
    //      'menu_name' => $value->menu_name,
    //      'title' => $value->link_title,
    //      'description' => '', // Options?
    //      'parent' => '', // How?
    //      'weight' => $value->weight,
    //    ];
    //
    //    return $return;
  }

}
