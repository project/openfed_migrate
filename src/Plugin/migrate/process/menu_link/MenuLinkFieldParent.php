<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\menu_link;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Language\LanguageInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\migrate\process\MenuLinkParent;
use Drupal\migrate\Row;
use Drupal\node\NodeInterface;

/**
 * Migrate data from Menu Link.
 *
 * @MigrateProcessPlugin(
 *   id = "menu_link_field_parent"
 * )
 */
class MenuLinkFieldParent extends MenuLinkParent {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // The field parentid always represents a reference to a nid because its
    // value is the result of a lookup on all node migrations.
    $parentId = $row->getDestinationProperty('parentid');

    // We also check for other parents, to make sure if this is a menu link or not.
    $parent_original = $row->getDestinationProperty('parent_original');
    $parent_translated = $row->getDestinationProperty('parent_translated');
    $parent_final_fixed = $row->getDestinationProperty('parent_final_fixed');

    if (!is_array($parentId)) {
      $parentId = [$parentId];
    }

    // Initial check of source variable to sanitize it.
    if (is_array($value[0])) {
      $value = $value[0];
    }
    if (!$value[0]) {
      // Top level item.
      return '';
    }

    // Check if the parent is a node or another menu item.
    // We can assure it's a node if the value of $value, a.k.a.
    // parent_node_or_menu, has the same id as parentID.
    // We add an extra check to make sure that $value is not in parent
    // destination properties, otherwise if $parentid and $value are the same
    // and $value is in parent_translated OR parent_original it means that the
    // nid is the same as mlid. In that case we should use the function
    // processMenuLinkParent because $value is a link.
    if ($parentId[0] == $value[0] && !in_array($value[0], [$parent_translated, $parent_original, $parent_final_fixed])) {
      // is a parent node
      return $this->processNodeParent($value[0], $value[1]);
    }
    else {
      // is a menu link parent
      return $this->processMenuLinkParent($value[0], $value[1]);
    }
  }

  private function processNodeParent($parent_id, $langcode) {

    $langcode = $langcode ? $langcode : LanguageInterface::LANGCODE_NOT_SPECIFIED;

    try {
      /** @var \Drupal\node\NodeStorage $parent_node */
      $parent_node = \Drupal::entityTypeManager()
        ->getStorage('node');

      $parent_node = $parent_node->load($parent_id);

      if (!$parent_node instanceof NodeInterface) {
        return '';
      }

      if ($parent_node->hasTranslation($langcode)) {
        $parent_node = $parent_node->getTranslation($langcode);
      }
      // As defined in Drupal\menu_link\Plugin\Field\FieldType\MenuLinkItem::getMenuPluginId()
      return 'menu_link_field:node_field_menulink_' . "{$parent_node->uuid()}_$langcode";

    } catch (InvalidPluginDefinitionException $e) {
    } catch (PluginNotFoundException $e) {
    }

    return '';
  }

  private function processMenuLinkParent($parent_id, $langcode) {

    $link = $this->menuLinkStorage->load($parent_id);
    if ($link) {
      /** @var \Drupal\Core\Menu\MenuLinkInterface $link */
      return $link->getPluginId();
    }

    // Root
    return '';
  }

}
