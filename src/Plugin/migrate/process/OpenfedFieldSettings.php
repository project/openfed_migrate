<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\field\Plugin\migrate\process\d7\FieldSettings;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "d7_field_settings"
 * )
 */
class OpenfedFieldSettings extends FieldSettings {

  /**
   * {@inheritdoc}
   *
   * This override will make sure that the target entity is block_content for
   * migrated beans.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $value = parent::transform($value, $migrate_executable, $row, $destination_property);

    switch ($row->getSourceProperty('type')) {
      case 'entityreference':
        if ($value['target_type'] == 'bean') {
          $value['target_type'] = 'block_content';
        }
        break;

      default:
        break;
    }

    return $value;
  }

}
