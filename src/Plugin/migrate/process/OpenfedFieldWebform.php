<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "openfed_field_webform"
 * )
 */
class OpenfedFieldWebform extends ProcessPluginBase {

  /**
   * Process to check if a value is empty, returning an alternative.
   *
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return 'webform_' . $value;
  }

}
