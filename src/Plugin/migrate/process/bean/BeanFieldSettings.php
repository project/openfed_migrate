<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\bean;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Configure field instance settings for beans.
 *
 * @MigrateProcessPlugin(
 *   id = "bean_field_settings"
 * )
 */
class BeanFieldSettings extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($row->getSourceProperty('type') == 'bean') {
      $value['target_type'] = 'block_content';
    }
    return $value;
  }

}
