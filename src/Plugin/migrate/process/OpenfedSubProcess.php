<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\migrate\process\SubProcess;
use Drupal\migrate\Row;

/**
 * @see \Drupal\migrate\Plugin\migrate\process\SubProcess
 *
 * @MigrateProcessPlugin(
 *   id = "sub_process",
 *   handle_multiples = TRUE
 * )
 */
class OpenfedSubProcess extends SubProcess {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $return = $source = [];

    if ($this->configuration['include_source']) {
      $key = $this->configuration['source_key'];
      $source[$key] = $row->getSource();
    }

    if (is_array($value) || $value instanceof \Traversable) {
      foreach ($value as $key => $new_value) {
        $new_row = new Row($new_value + $source);
        try {
          $migrate_executable->processRow($new_row, $this->configuration['process']);
          $destination = $new_row->getDestination();
          if (array_key_exists('key', $this->configuration)) {
            $key = $this->transformKey($key, $migrate_executable, $new_row);
          }
        } catch (MigrateSkipRowException $e) {
          // If MigrateExecutable::processRow() trows a SkipRow exception on
          // this subprocess then the "new_value" will be kept for field value.
          $destination = $new_value;
        }

        $return[$key] = $destination;
      }
    }
    return $return;
  }

}
