<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_plus\Plugin\migrate\process\DomMigrationLookup;

/**
 * String replacements on a source dom based on migration lookup.
 *
 * Meant to be used after dom process plugin.
 *
 * Available configuration keys:
 * - mode: What to modify. Possible values:
 *   - attribute: One element attribute.
 * - xpath: XPath query expression that will produce the \DOMNodeList to
 *   walk.
 * - attribute_options: A map of options related to the attribute mode. Required
 *   when mode is attribute. The keys can be:
 *   - name: Name of the attribute to match and modify.
 * - search: Regular expression to use. It should contain at least one
 *   parenthesized subpattern which will be used as the ID passed to
 *   migration_lookup process plugin.
 * - replace: Default value to use for replacements on migrations, if not
 *   specified on the migration. It should contain the '[mapped-id]' string
 *   where the looked-up migration value will be placed.
 * - migrations: A map of options indexed by migration machine name. Possible
 *   option values are:
 *   - replace: See replace option lines above.
 * - no_stub: If TRUE, then do not create stub entities during migration lookup.
 *   Optional, defaults to TRUE.
 *
 * Example:
 *
 * @code
 * process:
 *   'body/value':
 *     -
 *       plugin: dom
 *       method: import
 *       source: 'body/0/value'
 *     -
 *       plugin: dom_migration_lookup
 *       mode: attribute
 *       xpath: '//a'
 *       attribute_options:
 *         name: href
 *       search: '@/user/(\d+)@'
 *       replace: '/user/[mapped-id]'
 *       migrations:
 *         users:
 *           replace: '/user/[mapped-id]'
 *         people:
 *           replace: '/people/[mapped-id]'
 *     -
 *       plugin: dom
 *       method: export
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "dom_migration_lookup"
 * )
 */
class OpenfedDomMigrationLookup extends DomMigrationLookup {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $this->init($value, $destination_property);
    $this->transformParameters = [
      'migrate_executable' => $migrate_executable,
      'row' => $row,
      'destination_property' => $destination_property,
    ];

    foreach ($this->xpath->query($this->configuration['xpath']) as $html_node) {
      $subject = $this->getSubject($html_node);
      if (empty($subject)) {
        // Could not find subject, skip processing.
        continue;
      }
      $search = $this->getSearch();
      if (!preg_match($search, $subject, $matches)) {
        // No match found, skip processing.
        continue;
      }
      $id = $matches[1];
      // Walk through defined migrations looking for a map.
      foreach ($this->configuration['migrations'] as $migration_name => $configuration) {
        // We check the migration for the current node language, to get a better
        // match.
        $current_entity_language = $row->getSourceProperty('language');
        $mapped_id = $this->migrationLookup([$id, $current_entity_language], $migration_name);
        if (!is_null($mapped_id)) {
          // If the migration has more than one destination, we return only the
          // first one.
          // TODO: make this dynamic.
          if (count($mapped_id) > 1) {
            $mapped_id = $mapped_id[0];
          }
          // Not using getReplace(), since this implementation depends on the
          // migration.
          $replace = str_replace('[mapped-id]', $mapped_id, $configuration['replace']);
          $this->doReplace($html_node, $search, $replace, $subject);
          break;
        }
      }
    }

    return $this->document;
  }


}
