<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_plus\Plugin\migrate\process\Dom;

/**
 * Handles string to DOM and back conversions.
 *
 * @MigrateProcessPlugin(
 *   id = "dom"
 * )
 */
class OpenfedDom extends Dom {

  /**
   * {@inheritdoc}
   */
  public function export($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return str_replace("&#13;", "", parent::export($value, $migrate_executable, $row, $destination_property));
  }

  /**
   * {@inheritdoc}
   */
  public function import($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    // We add an extra check to see if the value is null. This may occur on
    // empty body fieds where body/value doesn't exist.
    if (is_null($value)) {
      $value = '';
    }

    $document = parent::import($value, $migrate_executable, $row, $destination_property);

    // Check for Image floats

    foreach ($document->getElementsByTagName('img') as $image) {
      if ($image->hasAttribute('style')) {
        $styles = explode(';', $image->getAttribute('style'));
        foreach ($styles as $index => $style) {
          if (preg_match('/float\W*\:\W*(left|right)/', $style, $matches)) {
            $image->setAttribute('data-align', $matches[1]);
            unset($styles[$index]);
            $image->setAttribute('style', implode(';', $styles));
            break;
          }
        }
      }
    }

    return $document;
  }

}
