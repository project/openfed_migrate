<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 * This is just a test process and doesn't perform any specific actions.
 *
 * @MigrateProcessPlugin(
 *   id = "transform_value"
 * )
 *
 */
class TransformValue extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    return parent::transfrom($value, $migrate_executable, $row, $destination_property);
  }

}
