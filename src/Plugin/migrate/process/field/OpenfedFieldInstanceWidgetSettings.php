<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\field;

use Drupal\field\Plugin\migrate\process\d6\FieldInstanceWidgetSettings;

/**
 * Get the field instance widget settings.
 *
 * @MigrateProcessPlugin(
 *   id = "field_instance_widget_settings"
 * )
 */
class OpenfedFieldInstanceWidgetSettings extends FieldInstanceWidgetSettings {

  /**
   * Merge the default D8 and specified D6 settings for a widget type.
   *
   * @param string $widget_type
   *   The widget type.
   * @param array $widget_settings
   *   The widget settings from D6 for this widget.
   *
   * @return array
   *   A valid array of settings.
   */
  public function getSettings($widget_type, $widget_settings) {
    if ($widget_type == 'inline_entity_form_complex') {
      $settings = [
        'inline_entity_form_complex' => [
          'allow_existing' => isset($widget_settings['type_settings']['allow_existing']) ? $widget_settings['type_settings']['allow_existing'] : '',
          'label_plural' => isset($widget_settings['type_settings']['label_plural']) ? $widget_settings['type_settings']['label_plural'] : '',
          'label_singular' => isset($widget_settings['type_settings']['label_singular']) ? $widget_settings['type_settings']['label_singular'] : '',
          'override_labels' => isset($widget_settings['type_settings']['override_labels']) ? $widget_settings['type_settings']['override_labels'] : '',
          'match_operator' => isset($widget_settings['type_settings']['match_operator']) ? $widget_settings['type_settings']['match_operator'] : '',
        ],
      ];

      return isset($settings[$widget_type]) ? $settings[$widget_type] : [];
    }

    return parent::getSettings($widget_type, $widget_settings);
  }

}
