<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\field;

use Drupal\file\Entity\File;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\field\Plugin\migrate\process\d7\FieldInstanceSettings;

/**
 * @MigrateProcessPlugin(
 *   id = "d7_field_instance_settings"
 * )
 */
class OpenfedFieldInstanceSettings extends FieldInstanceSettings {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    list($instance_settings, $widget_settings) = $value;
    $widget_type = $widget_settings['type'] ?? '';

    if ($widget_type === 'image_image') {
      $settings = $row->getSourceProperty('settings');
      $default_image = $settings['default_image'] ?? 0;

      if ($default_image !== 0) {
        /** @var \Drupal\file\Entity\File $file */
        $file = File::load($settings['default_image']);

        if ($file instanceof File) {
          $settings = $instance_settings;
          $settings['default_image'] = [
            'alt' => '',
            'title' => '',
            'width' => NULL,
            'height' => NULL,
            'uuid' => $file->uuid(),
          ];

          return $settings;
        }
      }
    }

    return parent::transform($value, $migrate_executable, $row, $destination_property);
  }

}
