<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\field;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\openfed_migrate\Helper;

/**
 * Get the coords from the simple gmap values.
 *
 * @MigrateProcessPlugin(
 *   id = "openfed_simple_gmap_to_address"
 * )
 */
class OpenfedSimpleGmapToAddress extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return [
      'country_code' => Helper::getCountry($value['value']),
      'address_line1' => $value['value'] ?? '',
    ];
  }

}
