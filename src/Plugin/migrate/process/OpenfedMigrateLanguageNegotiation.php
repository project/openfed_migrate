<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\language\Plugin\migrate\process\LanguageNegotiation;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Processes the arrays for the language types' negotiation methods and
 * weights.
 * This extends LanguageNegotiation in order to fix an issue with
 * language_cookie plugin naming.
 * TODO: the best is to suggest a patch for language_cookie instead.
 *
 * @MigrateProcessPlugin(
 *   id = "language_negotiation",
 *   handle_multiples = TRUE
 * )
 */
class OpenfedMigrateLanguageNegotiation extends LanguageNegotiation {

  /**
   * {@inheritdoc}
   * @throws \Drupal\migrate\MigrateException
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $new_value = parent::transform($value, $migrate_executable, $row, $destination_property);

    if (isset($new_value['enabled']['language_cookie'])) {
      $new_value['enabled']['language-cookie'] = $new_value['enabled']['language_cookie'];
      unset($new_value['enabled']['language_cookie']);
      asort($new_value['enabled']);
    }

    if (isset($new_value['method_weights']['language_cookie'])) {
      $new_value['method_weights']['language-cookie'] = $new_value['method_weights']['language_cookie'];
      unset($new_value['method_weights']['language_cookie']);
      asort($new_value['method_weights']);
    }

    return $new_value;
  }

  /**
   * Maps old negotiation method names to the new ones.
   *
   * @param string $value
   *   The old negotiation method name.
   *
   * @return string
   *   The new negotiation method name.
   */
  protected function mapNewMethods($value) {
    return parent::mapNewMethods($value);
  }

}
