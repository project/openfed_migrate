<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\menu_link_content;

use Drupal\menu_link_content\Plugin\migrate\process\LinkUri;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "link_uri"
 * )
 */
class OpenfedLinkUri extends LinkUri {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // We will add a try catch to avoid throwing an error if the path doesn't
    // validate. This path will be migrated later in some way so we don't need
    // to alarm the developer with errors :)
    $path = NULL;

    try {
      $path = parent::transform($value, $migrate_executable, $row, $destination_property);
    } catch (MigrateException $e) {
      return $path;
    }

    return $path;
  }

}
