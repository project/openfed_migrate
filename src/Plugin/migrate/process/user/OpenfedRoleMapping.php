<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\user;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Map old roles to new ones. This is pretty straight forward for default
 * Openfed7 > default Openfed8. For a specific project, you may want to extend
 * this class and add your own mapping.
 *
 * @MigrateProcessPlugin(
 *   id = "openfed_role_mapping"
 * )
 */
class OpenfedRoleMapping extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   *
   * Transform Content Manager into Content Editor.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return ($value == 'content_manager') ? 'content_editor' : $value;
  }

}
