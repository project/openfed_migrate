<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\block;

use Drupal\block\Plugin\migrate\process\BlockPluginId;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "block_plugin_id"
 * )
 */
class OpenfedBlockPluginId extends BlockPluginId {

  /**
   * {@inheritdoc}
   *
   * Set the menu and menu_block plugin id.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_array($value)) {
      list($module, $delta) = $value;
      if ($module == 'menu_block') {
        // Menu_block module is not ready for migrations so we need to manually
        // set the plugin id.
        $menu_name = $row->getSourceProperty('default_menu');
        if ($menu_name == 'multiple_menu_block') {
          return "multiple_menu_block:multiple_menu_block";
        }
        return "menu_block:$menu_name";
      }
      else {
        if ($module == 'menu') {
          // For menus, we'll transform them into menu_blocks instead of using
          // system menu_block because it doesn't provide a good compatibility
          // with future developments.
          return "menu_block:$delta";
        }
        elseif ($module == 'block') {
          $transformed_value = parent::transform($value, $migrate_executable, $row, $destination_property);
          // We need to run a migration lookup
          // because the parent class doesn't run
          // it on an migration with an "upgrade" prefix.
          if (empty($transformed_value)) {
            $lookup_result = $this->migrateLookup->lookup(['upgrade_d7_custom_block'], [$delta]);
            if ($lookup_result) {
              $block_id = $lookup_result[0]['id'];
            }
            if (!empty($block_id)) {
              return 'block_content:' . $this->blockContentStorage->load($block_id)->uuid();
            }
          }
        }
        elseif ($module == 'bean') {
          $transformed_value = parent::transform($value, $migrate_executable, $row, $destination_property);
          // For bean blocks we don't need to do lookup
          // and instead we generate the plugin id based
          // on the bid property of the current row.
          if (empty($transformed_value)) {
            return 'block_content:' . $this->blockContentStorage->load($row->getSourceProperty('bid'))->uuid();
          }
        }
        return parent::transform($value, $migrate_executable, $row, $destination_property);
      }
    }
    else {
      return $value;
    }
  }

}
