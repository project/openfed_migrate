<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\block;

use Drupal\block\Plugin\migrate\process\BlockSettings;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "block_settings"
 * )
 */
class OpenfedBlockSettings extends BlockSettings {

  /**
   * {@inheritdoc}
   *
   * Set the block configuration.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    list($plugin, $delta, $old_settings, $title) = $value;
    $settings = parent::transform($value, $migrate_executable, $row, $destination_property);

    // We've been storing settings_original for menu blocks to avoid overrides
    // since we want the settings as they are originally.
    if (strpos($plugin, 'menu_block') === 0 || strpos($plugin, 'multiple_menu_block') === 0) {
      $settings = $row->getSourceProperty('settings_original');
    }

    // Title and label display is being handled by the parent function but we
    // override it because it doesn't take in consideration the <none> attribute
    // that is often used in D7.
    // The real block title depends on the block type.
    if ($title == '<none>') {
      $settings['label_display'] = '0';
      // OpenfedBlock prepareRow already checks if title is <none> and sets an
      // alternative title to be used here.
      $title = $row->getSourceProperty('alternative_title');
      if ($title) {
        $settings['label'] = $title;
      }
      else {
        $settings['label'] = $plugin . ' - no administrative title';
      }
    }
    elseif (!empty($title)) {
      $settings['label'] = $title;
    }

    return $settings;
  }

}
