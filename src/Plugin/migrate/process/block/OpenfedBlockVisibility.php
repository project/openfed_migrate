<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\block;

use Drupal\block\Plugin\migrate\process\BlockVisibility;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "block_visibility"
 * )
 */
class OpenfedBlockVisibility extends BlockVisibility {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    list($old_visibility, $pages, $roles, $language, $content_type) = $value;
    $visibility = parent::transform($value, $migrate_executable, $row, $destination_property);

    // Setting the language visibility settings for blocks, if there is a source
    // 'language' property set (check OpenfedBlock class).
    if ($language) {
      $visibility['language'] = [
        'id' => 'language',
        'langcodes' => $language,
        'context_mapping' => [
          'language' => '@language.current_language_context:language_interface',
        ],
      ];
    }

    // Setting the language visibility settings for blocks, if there is a source
    // 'language' property set (check OpenfedBlock class).
    if ($content_type) {
      $visibility['entity_bundle:node'] = [
        'id' => 'entity_bundle:node',
        'bundles' => $content_type,
        'context_mapping' => [
          'node' => '@node.node_route_context:node',
        ],
      ];
    }

    return $visibility;
  }

}
