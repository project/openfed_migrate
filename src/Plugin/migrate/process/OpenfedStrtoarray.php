<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "openfed_strtoarray"
 * )
 */
class OpenfedStrtoarray extends ProcessPluginBase {

  /**
   * Converts a string to an array. If the source is not a string, returns it
   * unchanged.
   *
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_string($value)) {
      return [$value];
    }
    return $value;
  }

}
