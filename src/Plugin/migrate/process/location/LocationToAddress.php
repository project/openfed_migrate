<?php

namespace Drupal\openfed_migrate\Plugin\migrate\process\location;

use Drupal\Core\Database\Database;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\openfed_migrate\Helper;

/**
 * Maps D7 location values to D8 address values.
 *
 * Example:
 *
 * @code
 * process:
 *   field_address:
 *     plugin: location_to_address
 *     source: field_location
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "location_to_address"
 * )
 */
class LocationToAddress extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // $value is the array containing the lid for this location.
    $lid = $value['lid'];

    // Connect to the database defined by key 'migrate' (it would be
    // better to work off the configured connection, but I'm not sure how
    // to do that):
    Database::setActiveConnection('migrate');
    $db = Database::getConnection();
    $location = $db->select('location', 'l')
      ->where('l.lid = ' . $lid)
      ->fields('l', [
        'name',
        'street',
        'additional',
        'city',
        'province',
        'postal_code',
        'country',
      ])
      ->execute()
      ->fetchAssoc();
    Database::setActiveConnection();

    // Country is a mandatory value but not in D7. As such, we'll have to get it.
    $country = $location['country'];
    if (empty($country)) {
      $country = Helper::getCountry($location['street'] . ', ' . $location['city']);
    }

    $address = [
      'given_name' => '',
      'additional_name' => '',
      'family_name' => '',
      'organization' => $location['name'],
      'address_line1' => $location['street'],
      'address_line2' => $location['additional'],
      'postal_code' => $location['postal_code'],
      'sorting_code' => '',
      'dependent_locality' => '',
      'locality' => $location['city'],
      'administrative_area' => $location['province'],
      'country_code' => strtoupper($country),
    ];
    return $address;
  }

}
