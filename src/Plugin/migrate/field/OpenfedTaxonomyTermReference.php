<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\taxonomy\Plugin\migrate\field\TaxonomyTermReference;

/**
 * Makes some modifications to the plugin taxonomy_term_reference.
 */
class OpenfedTaxonomyTermReference extends TaxonomyTermReference {

  /**
   *
   * {@inheritdoc}
   *
   * The following override will take care of the conversion from
   * entityreference_view_widget to entity_browser plugin.
   */
  public function getFieldWidgetType(Row $row) {
    if ($row->getSourceProperty('widget/type') == 'entityreference_view_widget') {
      return 'entity_browser';
    }
    return $row->getSourceProperty('widget/type');
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $process = [
      'plugin' => 'sub_process',
      'source' => $field_name,
      'process' => [
        'target_id' => [
          [
            'plugin' => 'migration_lookup',
            'migration' => [
              'd7_taxonomy_term',
              'd7_taxonomy_term_translation'
            ],
            'source' => 'tid'
          ],
          [
            'plugin' => 'skip_on_empty',
            'method' => 'process'
          ],
          [
            'plugin' => 'entity_exists',
            'entity_type' => 'taxonomy_term'
          ],
          [
            'plugin' => 'skip_on_empty',
            'method' => 'process'
          ]
        ],
      ],
    ];
    $migration->setProcessOfProperty($field_name, $process);
  }


}
