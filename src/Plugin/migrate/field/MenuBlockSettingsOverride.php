<?php

namespace Drupal\field\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_drupal\Plugin\migrate\field\FieldPluginBase;

/**
 * MigrateField Plugin for Drupal 7 mbso fields.
 *
 * @MigrateField(
 *   id = "menu_block_settings_override",
 *   core = {7},
 *   type_map = {
 *     "menu_block_settings_override" = "node"
 *   },
 *   source_module = "menu_block_settings_override",
 *   destination_module = "core"
 * )
 */
class MenuBlockSettingsOverride extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $process = [
      'plugin' => 'sub_process',
      'source' => $field_name,
      'process' => [
        'value' => 'menu_block_settings_override',
      ],
    ];
    $migration->setProcessOfProperty($field_name, $process);
  }

}
