<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime\Plugin\migrate\field\DateField;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\MigrateException;

/**
 * Provides a field plugin for date and time fields.
 *
 * @MigrateField(
 *   id = "datetime",
 *   type_map = {
 *     "date" = "datetime",
 *     "datestamp" =  "timestamp",
 *     "datetime" =  "datetime",
 *   },
 *   core = {6,7},
 *   source_module = "date",
 *   destination_module = "datetime"
 * )
 */
class OpenfedDateField extends DateField {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $to_format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
    if (isset($data['field_definition']['data'])) {
      $field_data = unserialize($data['field_definition']['data']);
      if (isset($field_data['settings']['granularity'])) {
        $granularity = $field_data['settings']['granularity'];
        $collected_date_attributes = is_numeric(array_keys($granularity)[0])
          ? $granularity
          : array_keys(array_filter($granularity));
        if (empty(array_intersect($collected_date_attributes, ['hour', 'minute', 'second']))) {
          $to_format = DateTimeItemInterface::DATE_STORAGE_FORMAT;
        }
      }
    }

    // We must set a timezone. In D7 the timezone is set per field so we
    // should take that into consideration.
    // A "No time zone conversion" in D7 means that the date is saved as is
    // and in this case the field timezone_db is empty and we'll assume UTC
    // since this timezone doesn't suffer conversions in D7.
    // We only need the "to" timezone value, in D8 we always use the site
    // timezone, which is the default "from" timezone.
    // Database must store the value in UTC and UI will show it in default
    // timezone. This is the reason why we use the "to_timezone" instead of
    // "from_timezone", because our target timezone is the one used in D7.
    $field_timezone = $field_data['settings']['timezone_db'] ?: NULL;

    switch ($data['type']) {
      case 'date':
        $from_format = 'Y-m-d\TH:i:s';
        break;

      case 'datestamp':
        $from_format = 'U';
        $to_format = 'U';
        break;

      case 'datetime':
        $from_format = 'Y-m-d H:i:s';
        break;

      default:
        throw new MigrateException(sprintf('Field %s of type %s is an unknown date field type.', $field_name, var_export($data['type'], TRUE)));
    }
    $process = [
      'value' => [
        'plugin' => 'format_date',
        'from_format' => $from_format,
        'to_format' => $to_format,
        'source' => 'value',
      ],
    ];
    if ($field_timezone !== NULL) {
      $process['value']['to_timezone'] = $field_timezone ?: 'UTC';
    }

    $process = [
      'plugin' => 'sub_process',
      'source' => $field_name,
      'process' => $process,
    ];
    $migration->mergeProcessOfProperty($field_name, $process);
  }

}
