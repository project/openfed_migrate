<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\file\Plugin\migrate\field\d7\FileField;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;

/**
 * Class OpenfedFileField
 */
class OpenfedFileField extends FileField {

  /**
   * {@inheritdoc}
   */
  public function getFieldType(Row $row) {
    if ($row->getSourceProperty('widget_type') === 'enfield_widget') {
      return 'video_embed_field';
    }

    return parent::getFieldType($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldWidgetMap() {
    $widgets = parent::getFieldWidgetMap();
    $widgets['enfield_widget'] = 'video_embed_field_textfield';
    return $widgets;
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $widget_type = $data['widget']['type'] ?? '';
    if ($widget_type === 'enfield_widget') {
      $migration->mergeProcessOfProperty($field_name, [
        'plugin' => 'openfed_file_id_to_url',
        'source' => $field_name,
      ]);
    }
    else {
      parent::defineValueProcessPipeline($migration, $field_name, $data);
    }
  }

}
