<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\address\Plugin\migrate\field\AddressField;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * @MigrateField(
 *   id = "addressfield",
 *   core = {7},
 *   type_map = {
 *    "addressfield" = "address"
 *   },
 *   source_module = "addressfield",
 *   destination_module = "address"
 * )
 */
class OpenfedAddressField extends AddressField {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    // For location fields, we create a new field with the _ext suffix. We will
    // check if that suffix is present in the field name, meaning that it is the
    // field that we created. If that's the case, the source should be the
    // original field name.
    $address_field_override = $field_name;
    $plugin = 'addressfield'; // default address plugin.

    if(preg_match("#_ext$#", $field_name)) {
      $address_field_override = str_replace('_ext','', $field_name);
      $plugin = 'location_to_address';
    }
    $process = [
      'plugin' => $plugin,
      'source' => $address_field_override,
    ];
    $migration->mergeProcessOfProperty($field_name, $process);
  }

  /**
   * {@inheritdoc}
   */
  public function processFieldValues(MigrationInterface $migration, $field_name, $data) {
    $this->defineValueProcessPipeline($migration, $field_name, $data);
  }

}
