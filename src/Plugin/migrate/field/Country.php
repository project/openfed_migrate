<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\field\FieldPluginBase;

/**
 * Plugin to migrate from the Drupal 7 countries module.
 *
 * @MigrateField(
 *   id = "country",
 *   core = {7},
 *   type_map = {
 *     "country" = "list_string",
 *   },
 *   source_module = "countries",
 *   destination_module = "core",
 * )
 */
class Country extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFieldType(Row $row) {
    return 'list_string';
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldFormatterMap() {
    return [
      'country_default' => 'list_default',
      'country_default_i18n' => 'list_default',
      'country_official_i18n' => 'list_default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldWidgetMap() {
    return [
      'countries_continent' => 'options_select',
      'options_select' => 'options_select',
      'options_buttons' => 'options_buttons',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $migration->mergeProcessOfProperty($field_name, [
      'plugin' => 'sub_process',
      'source' => $field_name,
      'process' => [
        'value' => 'iso2'
      ],
    ]);
  }

}
