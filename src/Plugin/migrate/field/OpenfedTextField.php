<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\openfed_migrate\Helper;
use Drupal\text\Plugin\migrate\field\d7\TextField;

/**
 * Class OpenfedTextField
 */
class OpenfedTextField extends TextField {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    if (Helper::isMigrationFieldRowSimpleGmap($field_name)) {
      $migration->mergeProcessOfProperty($field_name, [
        'plugin' => 'openfed_simple_gmap_to_address',
        'source' => $field_name,
      ]);
    }
    else {
      parent::defineValueProcessPipeline($migration, $field_name, $data);
    }
  }

}
