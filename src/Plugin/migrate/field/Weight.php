<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_drupal\Plugin\migrate\field\FieldPluginBase;

/**
 * @MigrateField(
 *   id = "weight",
 *   core = {7},
 *   type_map = {
 *     "weight" = "weight"
 *   },
 *   source_module = "weight",
 *   destination_module = "weight"
 * )
 */
class Weight extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFieldWidgetMap() {
    return [
      'weight_selector' => 'weight_selector',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldFormatterMap() {
    return [
      'weight_integer' => 'default_weight',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $process = [
      'plugin' => 'sub_process',
      'source' => $field_name,
      'process' => [
        'value' => [
          'plugin' => 'get',
          'source' => 'weight',
        ],
      ],
    ];
    $migration->setProcessOfProperty($field_name, $process);
  }
}
