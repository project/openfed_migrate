<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_drupal\Plugin\migrate\field\FieldPluginBase;

/**
 * MigrateField Plugin for Drupal 7 geolocation field.
 *
 * @MigrateField(
 *   id = "geolocation",
 *   core = {7},
 *   type_map = {
 *     "geolocation" = "geolocation"
 *   },
 *   source_module = "geolocation",
 *   destination_module = "geolocation"
 * )
 */
class Geolocation extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function alterFieldWidgetMigration(MigrationInterface $migration) {
    parent::alterFieldWidgetMigration($migration);

    // set mapping for geolocation type.
    $process['type']['map']['location'] = 'geolocation_leaflet';
    $migration->mergeProcessOfProperty('options/type', $process);

  }

}
