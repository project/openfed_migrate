<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\paragraphs\Plugin\migrate\field\FieldCollection;

/**
 * Field Plugin for field collection migrations.
 * See FieldCollection class for more information.
 *
 * @see https://www.drupal.org/project/paragraphs/issues/2911244
 *
 * @MigrateField(
 *   id = "field_collection",
 *   core = {7},
 *   type_map = {
 *     "field_collection" = "entity_reference_revisions",
 *   },
 *   source_module = "field_collection",
 *   destination_module = "paragraphs",
 * )
 */
class OpenfedFieldCollection extends FieldCollection {

  /**
   * {@inheritdoc}
   */
  public function processFieldFormatter(MigrationInterface $migration) {
    parent::processFieldFormatter($migration);

    // Extra mappings that doesn't exist on parent method.
    $options_type[0]['map']['field_collection_view'] = 'entity_reference_revisions_entity_view';
    $options_type[1]['map']['field_collection_list'] = 'entity_reference_revisions_entity_view';
    $options_type[2]['map']['field_collection_fields'] = 'entity_reference_revisions_entity_view';
    $migration->mergeProcessOfProperty('options/type', $options_type);

  }

  /**
   * {@inheritdoc}
   */
  public function getFieldFormatterMap() {
    // Extra mappings that doesn't exist on parent method.
    return [
        'field_collection_view' => 'entity_reference_revisions_entity_view',
        'field_collection_list' => 'entity_reference_revisions_entity_view',
        'field_collection_fields' => 'entity_reference_revisions_entity_view',
      ] + parent::getFieldFormatterMap();
  }

  /**
   * {@inheritdoc}
   */
  public function processFieldValues(MigrationInterface $migration, $field_name, $data) {
    $this->defineValueProcessPipeline($migration, $field_name, $data);
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $process = [
      'plugin' => 'sub_process',
      'source' => $field_name,
      'process' => [
        'target_id' => [
          [
            'plugin' => 'paragraphs_lookup',
            'tags' => 'Field Collection Content',
            'source' => 'value',
          ],
          [
            'plugin' => 'extract',
            'index' => ['id'],
          ],
        ],
        'target_revision_id' => [
          [
            'plugin' => 'paragraphs_lookup',
            'tags' => [
              'Field Collection Revisions Content',
            ],
            'tag_ids' => [
              'Field Collection Revisions Content' => ['revision_id'],
            ],
          ],
          [
            'plugin' => 'extract',
            'index' => ['revision_id'],
          ],
        ],
      ],
    ];
    $migration->setProcessOfProperty($field_name, $process);

    // Add the respective field collection migration as a dependency.
    $dependencies = $migration->getMigrationDependencies();
    $migration_dependency = 'd7_field_collection:' . substr($field_name, static::FIELD_COLLECTION_PREFIX_LENGTH);
    $dependencies['required'][] = $migration_dependency;
    $migration_dependency = 'd7_field_collection_revisions:' . substr($field_name, static::FIELD_COLLECTION_PREFIX_LENGTH);
    $dependencies['required'][] = $migration_dependency;
    $migration->set('migration_dependencies', $dependencies);
  }

}
