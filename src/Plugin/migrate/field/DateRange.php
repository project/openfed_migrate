<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationDeriverTrait;
use Drupal\migrate_drupal\Plugin\migrate\field\FieldPluginBase;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Provides a field plugin for date and time fields.
 *
 * @MigrateField(
 *   id = "daterange",
 *   type_map = {
 *     "daterange" = "daterange",
 *     "datetime" = "datetime",
 *     "daterange_default" = "daterange_default",
 *     "daterange_datelist" = "daterange_datelist",
 *     "datestamp" = "datetime",
 *   },
 *   core = {7},
 *   source_module = "datetime",
 *   destination_module = "datetime_range"
 * )
 */
class DateRange extends FieldPluginBase {

  use MigrationDeriverTrait;

  /**
   * {@inheritdoc}
   */
  public function getFieldFormatterMap() {
    return [
      'daterange_default' => 'daterange_default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldWidgetMap() {
    return [
      'daterange_default' => 'daterange_default',
      'daterange_datelist' => 'daterange_datelist',
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    if (isset($data['field_definition']['data'])) {
      $type = $data['type'] ?? '';
      $field_data = unserialize($data['field_definition']['data']);
      $from_format = 'Y-m-d H:i:s';

      if ($type === 'datestamp') {
        $from_format = 'U';
      }

      $to_format = self::getDateTimeToFormat($field_name);

      // We must set a timezone. In D7 the timezone is set per field so we
      // should take that into consideration.
      // A "No time zone conversion" in D7 means that the date is saved as is
      // and in this case the field timezone_db is empty and we'll assume UTC
      // since this timezone doesn't suffer conversions in D7.
      // We only need the "to" timezone value, in D8 we always use the site
      // timezone, which is the default "from" timezone.
      // Database must store the value in UTC and UI will show it in default
      // timezone. This is the reason why we use the "to_timezone" instead of
      // "from_timezone", because our target timezone is the one used in D7.
      $field_timezone = $field_data['settings']['timezone_db'] ?: NULL;

      $process = [
        'value' => [
          'plugin' => 'format_date',
          'from_format' => $from_format,
          'to_format' => $to_format,
          'source' => 'value',
        ],
      ];
      if ($field_timezone !== NULL) {
        $process['value']['to_timezone'] = $field_timezone ?: 'UTC';
      }

      if (isset($field_data['settings']['todate']) && !empty($field_data['settings']['todate'])) {
        $process['end_value'] = [
          'plugin' => 'format_date',
          'from_format' => $from_format,
          'to_format' => $to_format,
          'source' => 'value2',
        ];

        if ($field_timezone !== NULL) {
          $process['end_value']['to_timezone'] = $field_timezone ?: 'UTC';
        }
      }

      $migration->mergeProcessOfProperty($field_name, [
        'plugin' => 'sub_process',
        'source' => $field_name,
        'process' => $process,
      ]);
    }
  }

  /**
   * Get the datetime storage format for a field.
   *
   * @param string $field_name
   *   The field name that is being processed.
   *
   * @return string
   *   The datetime storage format.
   */
  protected static function getDateTimeToFormat($field_name) {
    // Load the source plugin so we can check what format we can use.
    $d7_field_source = static::getSourcePlugin('d7_field');

    /* @var \Drupal\migrate\Row $row */
    foreach ($d7_field_source as $row) {
      $type = $row->getSourceProperty('type');

      // Only do it for the current field and date fields.
      if ($row->getSourceProperty('field_name') === $field_name && ($type === 'daterange' || $type === 'datestamp' || $type === 'datetime')) {
        $settings = $row->getSourceProperty('settings');

        // If all off these are 0 we can assume we only have to store date.
        $hour = $settings['granularity']['hour'] ?? 0;
        $minute = $settings['granularity']['minute'] ?? 0;
        $second = $settings['granularity']['minute'] ?? 0;

        if ($hour === 0 && $minute === 0 && $second === 0) {
          return DateTimeItemInterface::DATE_STORAGE_FORMAT;
        }
      }
    }

    return DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
  }

}
