<?php

namespace Drupal\openfed_migrate\Plugin\migrate\field;

use Drupal\migrate_drupal\Plugin\migrate\field\FieldPluginBase;

/**
 * Field Plugin for formatter migrations.
 *
 * @MigrateField(
 *   id = "formatter",
 *   core = {7},
 *   type_map = {
 *     "formatter" = "formatter_field_formatter",
 *   },
 *   source_module = "formatter_field",
 *   destination_module = "formatter_field",
 * )
 */
class Formatter extends FieldPluginBase {

}
